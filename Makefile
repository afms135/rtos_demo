#Name of binary
NAME = firmware

#C Sources
SRCS  = system/startup.c system/system.c
SRCS += $(wildcard hw/*.c)
SRCS += $(wildcard rtos/*.c)
SRCS += main.c lcd.c THP.c obj.c cmdshell.c

#Linker script
LNKS = system/link.ld

#Include directories
INCS = -I. -Isystem/CMSIS/Include -Isystem/stm32f3xx/Include

#Compiler defines
DEFS = -DSTM32F303x8

#Cortex M4 with Single Precision FPU
ARCH = -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16

#Retarget syscalls, newlib-nano
SPECS = --specs=nosys.specs --specs=nano.specs

#Warnings
WARNS = -Wall -Wextra -Wformat-overflow -Wformat-truncation -Wdouble-promotion -Wno-unused-parameter

#C compiler options
COPTS = -O3 -g3 -std=gnu11 -ffast-math -flto -funsigned-char -funsigned-bitfields -ffunction-sections -fdata-sections

#Linker options
LOPTS = -Wl,--gc-sections -lm -flto

################################################################################

#Cross compiler
PREFIX ?= arm-none-eabi-
CC = $(PREFIX)gcc
OBJCOPY = $(PREFIX)objcopy
SIZE = $(PREFIX)size

#Variable expansion
OBJS = $(SRCS:%.c=%.o)
DEPS = $(SRCS:%.c=%.d)
CFLAGS += $(ARCH) $(COPTS) $(WARNS) $(DEFS) $(INCS) $(SPECS)
LDFLAGS += $(ARCH) $(LOPTS) $(SPECS) -Wl,-Map='$(NAME).map' -T$(LNKS)

.PHONY: all clean

all: $(NAME).hex

$(NAME).hex: $(NAME).elf
	@echo '[OBJ] $@'
	@$(OBJCOPY) -O ihex $< $@
	@$(SIZE) -B $<

$(NAME).elf: $(OBJS)
	@echo '[LD] $@'
	@$(CC) $(LDFLAGS) -o $@ $^ -lm

%.o: %.c
	@echo '[CC] $@'
	@$(CC) $(CFLAGS) -MMD -MP -c $< -o $@

-include $(DEPS)

clean:
	rm -f $(OBJS) $(DEPS) $(NAME).hex $(NAME).elf $(NAME).map
