#include "os_cond.h"
#include "os_core.h"
#include "os_list.h"
#include "os_timer.h"

static void os_condcallback(void *data, void *data2)
{
	os_tcb *task = data;
	os_cond *cond = data2;
	os_listremovetcb(&cond->waiting, task);
	os_unblock(task, OS_TIMEOUT);
	os_sched();
}

void os_condinit(os_cond *c)
{
	os_listinit(&c->waiting);
}

os_error os_condwait(os_cond *c, os_mutex *m, uint32_t timeout)
{
	if(os_getrunningtask() == NULL)
		return OS_INVCXT;

	os_crit_t cr = os_entercritical();

	//Task should own the mutex before calling this function
	os_error ret = os_mutexunlock(m);
	if(ret != OS_OK)
	{
		os_exitcritical(cr);
		return ret;
	}

	//No timeout
	if(timeout == 0)
	{
		os_exitcritical(cr);
		os_mutexlock(m, OS_TIMEOUT_INF);
		return OS_TIMEOUT;
	}

	//Block
	os_tcb *task = os_getrunningtask();
	os_timer timer;
	timer.t = timeout;
	timer.callback = os_condcallback;
	timer.data = task;
	timer.data2 = c;
	os_block(WAIT_COND, &c->waiting, (timeout != OS_TIMEOUT_INF) ? &timer: NULL);
	os_exitcritical(cr);

	//Regain mutex
	os_mutexlock(m, OS_TIMEOUT_INF);
	return task->ret;
}

void os_condsignal(os_cond *c)
{
	os_crit_t cr = os_entercritical();
	if(!os_listempty(&c->waiting))
	{
		os_unblock(os_listdequeue(&c->waiting), OS_OK);
		os_sched();
	}
	os_exitcritical(cr);
}

void os_condbroadcast(os_cond *c)
{
	os_crit_t cr = os_entercritical();
	while(!os_listempty(&c->waiting))
		os_unblock(os_listdequeue(&c->waiting), OS_OK);
	os_sched();
	os_exitcritical(cr);
}
