#ifndef OS_LIST_H_
#define OS_LIST_H_

#include "os.h"

/**
 * \addtogroup List
 * \brief Functions to manipulate \c ::os_tcblist linked lists
 *
 * \c ::os_tcb structures can only be in one list at any one time.
 * @{
 * \file
 */

/**
 * \brief Initialise a list
 *
 * Initialises a \c ::os_tcblist structure by setting the head and tail to NULL.
 *
 * \param[in] *list Pointer to list to be initialised
 */
void os_listinit(os_tcblist *list);

/**
 * \brief Enqueue a tcb to list tail
 *
 * Example:
 * \code
 * os_tcblist list;
 * os_listinit(&list);
 * os_tcb *task = os_getrunningtask();
 *
 * os_listenqueue(&list, task);
 * \endcode
 * \param[in] *list Pointer to list to be updated
 * \param[in] *task Pointer to task to enqueue
 */
void os_listenqueue(os_tcblist *list, os_tcb *task);

/**
 * \brief Dequeue a tcb from list head
 *
 * Returns a pointer to the dequeued \c os_tcb.
 * \param[in] *list Pointer to list to dequeue from
 */
os_tcb *os_listdequeue(os_tcblist *list);

/**
 * \brief Check if list is empty
 *
 * Returns \c 1 if list is empty, \c 0 otherwise.
 * \param[in] *list Pointer to list to check
 */
int os_listempty(os_tcblist *list);

/**
 * \brief Remove tcb from list
 *
 * Removes the given tcb from the given licked list.
 *
 * \warning The tcb has to be in the list given or
 * risk leaving the list in an inconsistent state.
 * \param[in] *list Pointer to list to remove from
 * \param[in] *task Pointer to task the remove
 */
void os_listremovetcb(os_tcblist *list, os_tcb *task);

/**@}*/
#endif /* OS_LIST_H_ */
