#ifndef OS_CONF_H_
#define OS_CONF_H_

/**
 * \addtogroup Configuration
 * \brief Configurable parameters
 *
 * These preprocessor parmeters are compile-time constants
 * used to modify the runtime behaviour of the OS.
 *
 * @{
 * \file
 */

/**
 * \brief CPU Frequency
 *
 * The microcontroller CPU Frequency measured in hertz.
 *
 * Used to configure the OS timer.
 */
#define OS_CPU_FREQ (64000000)

/**
 * \brief OS tick length
 *
 * The number of microseconds a tick lasts.
 */
#define OS_TICK_TIMEUS (1000)

/**
 * \brief OS time slice length
 *
 * The number of ticks before a task is preempted.
 */
#define OS_TIME_SLICE (10)

/**@}*/
#endif /* OS_CONF_H_ */
