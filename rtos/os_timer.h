#ifndef OS_TIMER_H_
#define OS_TIMER_H_

#include "os.h"
#include "os_conf.h"

/**
 * \addtogroup Timer
 * \brief Timer callback management
 *
 * A Timer timeout is measured in ticks, the callback
 * takes 2 \c void pointer arguments and is of the form:
 * \code
 * void callback(void *data, void *data2);
 * \endcode
 *
 * Timers should not call \c ::os_sched() if a task wakeup is
 * desired, it is called if necessary at the end of the timer ISR
 *
 * \warning The timer callback is called within a critical section.
 * \sa os_timer
 * @{
 * \file
 */

/**
 * \brief Infinite timeout
 *
 * Value that can be used to indicate that a task is never to be woken up.
 * \warning Not all APIs support this value.
 */
#define OS_TIMEOUT_INF (0xFFFFFFFF)

/**
 * \brief Convert OS ticks to microseconds
 */
#define OS_TICKSTOUS(t) ((t)*OS_TICK_TIMEUS)

/**
 * \brief Convert OS ticks to milliseconds
 */
#define OS_TICKSTOMS(t) ((t)*(OS_TICK_TIMEUS/1000))

/**
 * \brief Convert microseconds to OS ticks
 * \warning If <tt>t < ::OS_TICK_TIMEUS</tt> this macro expands to 0.
 */
#define OS_USTOTICKS(t) ((t)/OS_TICK_TIMEUS)

/**
 * \brief Convert milliseconds to OS ticks
 * \warning If <tt>t < ::OS_TICK_TIMEUS/1000</tt> this macro expands to 0.
 */
#define OS_MSTOTICKS(t) ((t)/(OS_TICK_TIMEUS/1000))

/**
 * \brief Register timer
 *
 * Adds the given timer to the timer list,
 * when the timeout equals zero the callback function is called.
 *
 * \warning \c tim->t cannot be \c ::OS_TIMEOUT_INF or \c 0 or
 * the timeout will delay for the maximum amount of time.
 * \warning Callback is called in an interrupt context.
 * \param[in] *tim Pointer to timer to enqueue
 */
void os_timerenqueue(os_timer *tim);

/**
 * \brief Cancel timer
 *
 * Removes the given timer from the timer list.
 * \warning Does not call the timer callback.
 * \param[in] *tim Pointer to timer to cancel
 */
void os_timercancel(os_timer *tim);

/**
 * \brief Suspend task
 *
 * Suspends the calling task for \c timeout ticks.
 *
 * If <tt>timeout == ::OS_TIMEOUT_INF</tt> the task is suspended indefinitely.
 * \warning Cannot be called from an ISR.
 * \param[in] timeout Ticks to suspend task for
 * \retval ::OS_OK Success
 * \retval ::OS_INVCXT Function was called from an ISR
 */
os_error os_suspend(uint32_t timeout);

/**
 * \brief Wakeup task that was suspended indefinitely
 *
 * Only works for tasks that were suspended using
 * os_suspend with \c ::OS_TIMEOUT_INF.
 *
 * \param[in] *task Task to be woken up
 * \retval ::OS_OK Success
 * \retval ::OS_INVPARAM Task was not suspended to the above criteria
 */
os_error os_wakeup(os_tcb *task);

/**
 * \brief Update timer list
 *
 * Decrements the timeout value of each timer in the timer list.
 *
 * When the timeout value equals \c 0 the callback is called.
 * \warning Used in ::os_tickhandler, not to be called from application code.
 */
void os_updatetimers(void);

/**@}*/
#endif /* OS_TIMER_H_ */
