#ifndef OS_ARCH_H_
#define OS_ARCH_H_

#include <stdint.h>
#include <stddef.h>

/**
 * \addtogroup Arch
 * \brief Architecture specific functions and definitions
 *
 * \warning These functions are internal and should not
 * be called from application code.
 * @{
 * \file
 */

/**
 * \brief Platform name
 *
 * The name of the OS port platform.
 */
#ifdef __ARM_ARCH_6M__
	#define OS_PLATFORM "ARM Cortex-M0/M0+"
#elif __ARM_ARCH_7M__
	#define OS_PLATFORM "ARM Cortex-M3"
#elif __ARM_ARCH_7EM__
	#ifndef __ARM_FP
		#define OS_PLATFORM "ARM Cortex-M4/M7"
	#else
		#define OS_PLATFORM "ARM Cortex-M4F/M7F"
	#endif
#endif

/**
 * \brief Architecture name
 *
 * The name of the OS port CPU architecture.
 */
#ifdef __ARM_ARCH_6M__
	#define OS_ARCHITECTURE "ARMv6-M"
#elif __ARM_ARCH_7M__
	#define OS_ARCHITECTURE "ARMv7-M"
#elif __ARM_ARCH_7EM__
	#define OS_ARCHITECTURE "ARMv7E-M"
#endif

/**
 * \brief Stack alignment for architecture
 *
 * The alignment in bytes that the ::OS_STACK array needs to be aligned to.
 */
#define OS_STACK_ALIGN (8)

/**
 * \brief Minimum stack size
 *
 * The stack will need to be larger for most practical applications.
 * \warning The stack will need to be larger if it is not aligned to
 *          ::OS_STACK_ALIGN bytes.
 */
#define OS_ARCH_MIN_STACK (68)

/**
 * \brief Critital section state
 *
 * Type large enough to hold current interrupt state.
 */
typedef uint32_t os_crit_t;

/**
 * \brief Enter critical section
 *
 * Used before code where preemption cannot occur.
 *
 * Example:
 * \code
 * os_crit_t c = os_entercritical();
 * //Critical section
 * os_exitcritical(c);
 * \endcode
 * \sa os_exitcritical
 */
os_crit_t os_entercritical(void);

/**
 * \brief Exit critical section
 *
 * Used after code where preemption cannot occur.
 * \sa os_entercritical
 */
void os_exitcritical(os_crit_t c);

/**
 * \brief Perform a context switch
 *
 * When called it will stop execution of the running task then
 * switch execution to the task specified in os_nexttask.
 * \warning Must be called within a critical section.
 */
void os_cxtswitch(void);

/**
 * \brief Perform a context switch from an ISR
 *
 * Potentially called at the end of an ISR if a different
 * task has been made ready to run by an ISR.
 * \warning Must be called within a critical section.
 */
void os_cxtswitchisr(void);

/**
 * \brief Wait for interrupt
 *
 * An architecture specific sequence that waits for the next
 * interrupt and potentially places the CPU into a low power state.
 *
 * Used in the idle task for power saving.
 */
void os_wfi(void);

/**
 * \brief Find last set bit in byte
 *
 * Returns the bit number (0-indexed) of the highest set bit in a 8-bit value.
 * Used for finding the highest priority task in priority lists.
 *
 * \warning Behaviour when <tt>v == 0</tt> is undefined.
 * \param[in] v 8-bit value to search
 * \retval uint8_t Bit number of the highest set bit (Where \c 0x80 returns \c 7,
 *                 \c 0x01 returns \c 0)
 */
uint8_t os_fls(uint8_t v);

/**
 * \brief Starts the OS scheduler
 *
 * Sets up the tick interrupt handler, enables interrupts
 * then starts the first task, given in os_running.
 *
 * \warning Does not return.
 */
void os_archstart(void);

/**
 * \brief Initialises the stack for a task
 *
 * Takes a pointer to the top of the stack, aligns it to ::OS_STACK_ALIGN bytes, sets up the
 * stack frame at the bottom of the stack then returns a pointer to the top
 * of the stack frame.
 *
 * It also writes the stack base address and size to the caller.
 *
 * \param[in] *stack Pointer to the top of the stack
 * \param[in] size Size of the stack in bytes
 * \param[in] *func Pointer to the task
 * \param[in] *arg Argument pointer to pass to the task
 * \param[out] *stk_base Base of stack
 * \param[out] *stk_size Size of stack
 */
uintptr_t os_stackinit(void *stack, size_t size, void(*func)(void*), void *arg, uintptr_t *stk_base, size_t *stk_size);

/**@}*/
#endif /* OS_ARCH_H_ */
