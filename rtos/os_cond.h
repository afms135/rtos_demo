#ifndef OS_COND_H_
#define OS_COND_H_

#include "os.h"
#include "os_mutex.h"

/**
 * \addtogroup Condvar Condition Variable
 * \brief Functions for condition signaling and waiting
 *
 * Provides blocking condition variables with optional timeouts.
 * @{
 * \file
 */

/**
 * \brief OS Condition Variable type
 */
typedef struct os_cond
{
	os_tcblist waiting; ///<List of tasks waiting on condition variable
} os_cond;

/**
 * \brief Initialise condition variable
 *
 * Must be called before using the condition variable.
 * \param[in] *c Pointer to condition variable
 */
void os_condinit(os_cond *c);

/**
 * \brief Macro to initialise a condition variable
 *
 * Macro to initialise a condition variable statically at compile time,
 * used as an alternative to ::os_condinit().
 */
#define OS_CONDINIT {{NULL, NULL}}

/**
 * \brief Wait for condition
 *
 * First the mutex is unlocked, then the behaviour depends on the value of
 * \c timeout:
 *
 * - <tt>timeout == 0</tt>
 * 	- The mutex is reaquired and the fuction returns \c ::OS_TIMEOUT.
 * - <tt>timeout > 0</tt>
 * 	- The task is suspended until the condition is signaled or timeout occurs.
 * - <tt>timeout == ::OS_TIMEOUT_INF</tt>
 * 	- The task is suspended indefinitely until the condition is signaled.
 *
 * After the task is unblocked the mutex is reaquired.
 *
 * \warning Function cannot be called from an ISR.
 * \warning Mutex must be locked before calling this function.
 * \param[in] *c Pointer to condition variable
 * \param[in] *m Pointer to mutex
 * \param[in] timeout Timeout to wait for condition
 * \retval ::OS_OK Success
 * \retval ::OS_INVCXT Function was called from an ISR
 * \retval ::OS_OWNED Calling task not owner of mutex
 * \retval ::OS_INVPARAM Mutex was not locked before calling function
 * \retval ::OS_TIMEOUT Timeout occurred before condition was signalled
 */
os_error os_condwait(os_cond *c, os_mutex *m, uint32_t timeout);

/**
 * \brief Signal a single task
 *
 * Unblocks the first task waiting on the condition variable (FIFO).
 *
 * \param[in] *c Pointer to condition variable
 */
void os_condsignal(os_cond *c);

/**
 * \brief Signal all tasks
 *
 * Unblocks all tasks waiting on the condition variable.
 *
 * \param[in] *c Pointer to condition variable
 */
void os_condbroadcast(os_cond *c);

/** @}*/
#endif /* OS_COND_H_ */
