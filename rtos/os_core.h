#ifndef OS_CORE_H_
#define OS_CORE_H_

#include "os.h"
#include "os_arch.h"

/**
 * \addtogroup Core
 * \brief OS task management
 *
 * Core functions to initialise and start OS, task creation and scheduling.
 * @{
 * \file
 */

/**
 * \brief Macro to define a stack for a task
 *
 * A stack is \c size \c + \c sizeof(os_tcb) large,
 * due to the TCB being stored at the top of the array.
 *
 * Example:
 * \code
 * OS_STACK(task_stk, 256); \\Define a 256 byte stack
 * void task(void *val) \\Associated task
 * {
 * 	while(1);
 * }
 * \endcode
 *
 * \param[in] name Name of the created array
 * \param[in] size Size of the task stack in bytes
 */
#define OS_STACK(name,size) \
        __attribute__((aligned(OS_STACK_ALIGN))) \
        uint8_t name[size+sizeof(os_tcb)]

/**
 * \brief Macro to create a task
 *
 * A convenience macro to initialise a task function
 * and its associated stack.
 *
 * Creates a \c ::OS_STACK array called \c *name*_stk and
 * an accompanying function called \c *name*_task.
 *
 * Can be used in conjunction with \c ::OS_TASKINIT.
 *
 * \param[in] name Task name
 * \param[in] size Size of the task stack in bytes
 * \param[in] arg  Name of the task argument variable
 */
#define OS_TASK(name,size,arg) \
        OS_STACK(name##_stk, size); \
        void name##_task(void *arg)

/**
 * \brief Macro to initialise a task
 *
 * A convenience macro to initialise a task,
 * fills in the common \c ::os_taskinit parameters.
 *
 * \warning This macro only works for tasks
 * created using the \c ::OS_TASK macro.
 *
 * \param[in] name Task name
 * \param[in] arg  Task argument
 * \param[in] prio Task priority
 * \param[in] desc Task description
 */
#define OS_TASKINIT(name,arg,prio,desc) \
        os_taskinit(name##_task, arg, prio, name##_stk, sizeof(name##_stk), desc);

/**
 * \brief Initialise OS
 *
 * Sets up the idle task and internal lists.
 *
 * \warning Must be called before all other OS functions.
 * \warning Disables interrupts.
 * \param[in] *idle_stk Pointer to the idle stack
 * \param[in] idle_size Size of the idle task stack in bytes
 * \retval ::OS_OK Success
 * \retval ::OS_INVPARAM Null pointer passed or stack too small
 */
os_error os_init(void *idle_stk, size_t idle_size);

/**
 * \brief Start OS scheduler
 *
 * Enables interrupts, sets the first task to run and starts scheduling.
 * \warning Does not return.
 */
void os_start(void);

/**
 * \brief Initialise task
 *
 * Sets up the task stack, \c ::os_tcb, and inserts it into the ready list.
 *
 * Example:
 * \code
 * OS_STACK(task_stk, 128);
 * void task(void *v)
 * {
 * 	while(1);
 * }
 *
 * void main(void)
 * {
 * 	...
 * 	os_taskinit(task, 0, task_stk, sizeof(task_stk), "Task");
 * 	...
 * }
 * \endcode
 *
 * \warning Tasks must loop indefinitely or call ::os_exit() at the end
 *          of the task otherwise undefined behaviour will occur.
 * \param[in] *func Pointer to task
 * \param[in] *arg Pointer to task argument
 * \param[in] prio Task priority
 * \param[in] *stack Pointer to task stack
 * \param[in] size Size of task stack in bytes
 * \param[in] *desc Pointer to a string describing the task
 * (\c NULL for no description)
 * \retval ::OS_OK Success
 * \retval ::OS_INVPARAM Null pointers passed, stack too small
 *         or a priority above ::OS_MAX_PRIO pas passed
 */
os_error os_taskinit(void(*func)(void*), void *arg, uint8_t prio, void *stack, size_t size, const char *desc);

/**
 * \brief Terminate task
 *
 * Stops the calling task, cancels any timer it is waiting on
 * and removes it from the ready list permanently.
 * \warning Function cannot be called from an ISR.
 */
void os_exit(void);

/**
 * \brief Perform a context switch
 *
 * Switches out the currently running task if there is a higher priority task
 * in the ready list or the current time slice is up. Tasks of equal priority
 * are scheduled in a round-robin fashion.
 *
 * \warning An ::os_exitcritical() call should always follow this function call.
 * \warning Does nothing in an ISR.
 * \warning Must be called within a critical section.
 */
void os_sched(void);

/**
 * \brief Block task
 *
 * Suspends the currently running task, changes its state, optionally enqueuing
 * the running task on a ::os_tcblist and optionally enqueuing a ::os_timer
 * timeout structure.
 *
 * \warning An ::os_exitcritical() call should always follow this function call
 *          and the ::os_timer struct must be in scope between these calls.
 * \param[in] state New state
 * \param[in] *list List to enqueue running task (\c NULL to not enqueue task to list)
 * \param[in] *timer Timer to register (\c NULL to not register a timer)
 */
void os_block(os_taskstate state, os_tcblist *list, os_timer *timer);

/**
 * \brief Unblock task
 *
 * Unblocks a given task, setting its state to ::READY, inserting it into
 * the ready list. Sets the \c ret field in the ::os_tcb which is used by
 * the task to determine the cause of the wakeup. Cancels any timer
 * associated with the task.
 *
 * \warning Must be called within a critical section.
 * \param[in] *task Task to be made ready to run
 * \param[in] ret An ::os_error code to set in the ::os_tcb
 */
void os_unblock(os_tcb *task, os_error ret);

/**
 * \brief Change current running task priority
 *
 * \param[in] new_p New task priority
 * \param[in] *old_p Pointer to store current task priority,
 *            \c NULL if not required
 * \retval ::OS_OK Success
 * \retval ::OS_INVCXT Function was called from an ISR
 * \retval ::OS_INVPARAM New priority above ::OS_MAX_PRIO pas passed
 */
os_error os_chprio(uint8_t new_p, uint8_t *old_p);

/**
 * \brief Get task control block
 *
 * Return the \c ::os_tcb of the calling task, \c NULL if called from an ISR.
 * \warning Will not return \c NULL in an ISR if os_enterirq and os_exitirq
 * are not called.
 */
os_tcb *os_getrunningtask(void);

/**
 * \brief Get Process ID
 *
 * Returns the Process ID of the calling task,
 * returns \c 0 if called from an ISR.
 * \warning Function cannot be used in the idle task
 * due to the idle task having a PID of \c 0.
 */
uint8_t os_getpid(void);

/**
 * \brief Iterate over master task list
 *
 * When called with \c NULL the first task is returned.
 *
 * When called with a pointer to the last task returned,
 * the next task is returned or \c NULL if no tasks remain.
 *
 * Example:
 * \code
 * os_tcb *iter = os_itertasks(NULL);
 * while(iter != NULL)
 * {
 * 	//Use tcb
 * 	iter = os_itertasks(iter);
 * }
 * \endcode
 * \param[in] *from Task to iterate from, or \c NULL for the first task
 */
os_tcb *os_itertasks(os_tcb *from);

/**
 * \brief Get ticks
 *
 * Returns the number of ticks that have passed since os_start.
 */
uint64_t os_getticks(void);

/**
 * \brief Get idle ticks
 *
 * Returns the number of ticks that have passed running the idle task.
 */
uint64_t os_getidleticks(void);

/**
 * \brief Return the \c ::os_tcb for the given PID
 *
 * If there is no task with the given PID \c NULL is returned.
 */
os_tcb *os_gettcb(uint8_t pid);

/**
 * \brief Start ISR
 *
 * Must be called at the beginning of ISRs that call OS functions.
 * \sa os_exitirq
 */
void os_enterirq(void);

/**
 * \brief End ISR
 *
 * Must be called at the end of ISRs that have called ::os_enterirq.
 * \sa os_enterirq
 */
void os_exitirq(void);

/**
 * \brief Updates OS tick state
 *
 * Increments the ticks passed since start and time slice timer,
 * if \c ::OS_TIME_SLICE ticks have passed the currently running task
 * is preempted.
 * \warning Used in tick ISR, should not be called from application code.
 */
void os_tickhandler(void);

/**@}*/
#endif /* OS_CORE_H_ */
