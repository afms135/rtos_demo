#include "os_core.h"
#include "os_arch.h"
#include "os_list.h"
#include "os_prio.h"
#include "os_timer.h"

//
// Kernel internal variables
//
uint8_t os_started = 0;  //Flag set when OS is started
uint8_t os_ntasks = 0;   //Number of tasks
uint8_t os_irqnest = 0;  //Number of nested IRQ handlers

//
// OS timing variables
//
uint64_t os_ticks = 0;     //Number of OS ticks since start
uint64_t os_idleticks = 0; //Number of OS ticks spent running idle task
uint8_t os_tim_slice = 0;  //Timer value for context switching

//
// Context switch variables
//
os_tcb *os_running = NULL;  //Current running task, set by arch layer
os_tcb *os_nexttask = NULL; //Next task to be run
uint8_t os_schedrr = 0;     //Round robin tasks of equal priority

//
// OS linked lists
//
os_priolist os_readylist; //List of tasks ready to run
os_tcblist os_masterlist; //Master list of all tasks

static void os_idletask(void *arg)
{
	(void)arg; //Suppress warning
	while(1)
		os_wfi();
}

static int os_schedcore(void)
{
	if(os_running->state == RUNNING) //Task is being preempted
		os_running->state = READY;

	if(os_schedrr)
	{
		os_schedrr = 0;
		//Round-robin
		os_tcb *t = os_priolistdequeue(&os_readylist);
		os_priolistenqueue(&os_readylist, t);
	}
	os_nexttask = os_priolistpeek(&os_readylist);

	if(os_running == os_nexttask) //No switch required
	{
		os_running->state = RUNNING; //Current task is running
		return 0;
	}
	else
		return 1;
}

os_error os_init(void *idle_stk, size_t idle_size)
{
	os_entercritical(); //Disable interrupts before OS start
	os_started = 0;
	os_priolistinit(&os_readylist);
	os_listinit(&os_masterlist);
	return os_taskinit(os_idletask, 0, OS_MIN_PRIO, idle_stk, idle_size, "Idle");
}

void os_start(void)
{
	os_started = 1;
	os_nexttask = os_priolistpeek(&os_readylist);
	os_archstart(); //Enables interrupts, does not return
}

os_error os_taskinit(void(*func)(void*), void *arg, uint8_t prio, void *stack, size_t size, const char *desc)
{
	if(stack == NULL || func == NULL)
		return OS_INVPARAM;
	if(size < OS_ARCH_MIN_STACK + sizeof(os_tcb)) //Stack too small
		return OS_INVPARAM;
	if(prio > OS_MAX_PRIO)
		return OS_INVPARAM;

	os_crit_t c;
	if(os_started)
		c = os_entercritical();

	os_tcb *task = stack; //TCB is stored at top of stack array
	size -= sizeof(os_tcb);
	task->SP = os_stackinit(&task->stk_top, size, func, arg, &task->stk_base, &task->stk_size);
	task->PID = os_ntasks++;
	task->prio = prio;
	task->state = READY;
	task->desc = (desc == NULL) ? "" : desc;

	task->timer = NULL;
	task->ret = OS_OK;

	if(os_listempty(&os_masterlist))
	{
		os_masterlist.head = task;
		os_masterlist.tail = task;
		task->m_next = NULL;
		task->m_prev = NULL;
	}
	else
	{
		task->m_next = NULL;
		task->m_prev = os_masterlist.tail;
		os_masterlist.tail->m_next = task;
		os_masterlist.tail = task;
	}
	os_priolistenqueue(&os_readylist, task);

	if(os_started)
	{
		os_sched();
		os_exitcritical(c);
	}
	return OS_OK;
}

void os_exit(void)
{
	if(os_getrunningtask() == NULL) //Not in task context
		return;

	os_crit_t c = os_entercritical();
	if(os_running->timer != NULL)
		os_timercancel(os_running->timer);
	os_block(KILLED, NULL, NULL);
	os_exitcritical(c);
}

void os_sched(void)
{
	if(os_irqnest) //In ISR, switch at os_exitirq()
		return;
	if(os_schedcore())
		os_cxtswitch();
}

void os_block(os_taskstate state, os_tcblist *list, os_timer *timer)
{
	os_priolistdequeue(&os_readylist);
	os_running->state = state;
	if(list != NULL)
		os_listenqueue(list, os_running);

	os_running->timer = timer;
	if(timer != NULL)
		os_timerenqueue(timer);
	os_running->ret = OS_OK; //Changed by callback

	os_sched();
}

void os_unblock(os_tcb *task, os_error ret)
{
	task->state = READY;
	task->ret = ret;
	if(task->timer != NULL)
	{
		os_timercancel(task->timer);
		task->timer = NULL;
	}
	os_priolistenqueue(&os_readylist, task);
}

os_error os_chprio(uint8_t new_p, uint8_t *old_p)
{
	if(os_getrunningtask() == NULL)
		return OS_INVCXT;
	if(new_p > OS_MAX_PRIO)
		return OS_INVPARAM;

	os_crit_t c = os_entercritical();

	os_tcb *t = os_priolistdequeue(&os_readylist);
	if(old_p != NULL)
		*old_p = t->prio;
	t->prio = new_p;
	os_priolistenqueue(&os_readylist, t);

	os_sched();
	os_exitcritical(c);
	return OS_OK;
}

void os_tickhandler(void)
{
	os_enterirq();
	os_crit_t c = os_entercritical();

	os_ticks++;
	if(os_running->PID == 0) //Idle task
		os_idleticks++;
	os_tim_slice++;
	os_updatetimers();

	if(os_tim_slice == OS_TIME_SLICE)
	{
		os_tim_slice = 0; //Reset timer
		os_schedrr = 1;
	}

	os_exitcritical(c);
	os_exitirq();
}

os_tcb *os_getrunningtask(void)
{
	//No critical section required because os_tcb's do not move in memory
	//and from task perspective os_running will point to itself.
	return (os_irqnest) ? NULL : os_running;
}

uint8_t os_getpid(void)
{
	os_tcb *t = os_getrunningtask();
	return (t != NULL) ? t->PID : 0;
}

os_tcb *os_itertasks(os_tcb *from)
{
	//Master list is constant, head will always point to idle task tcb
	if(from == NULL)
		return os_masterlist.head;
	os_crit_t c = os_entercritical();
	os_tcb *ret = from->m_next;
	os_exitcritical(c);
	return ret;
}

uint64_t os_getticks(void)
{
	os_crit_t c = os_entercritical();
	uint64_t ret = os_ticks;
	os_exitcritical(c);
	return ret;
}

uint64_t os_getidleticks(void)
{
	os_crit_t c = os_entercritical();
	uint64_t ret = os_idleticks;
	os_exitcritical(c);
	return ret;
}

os_tcb *os_gettcb(uint8_t pid)
{
	os_tcb *iter = os_itertasks(NULL);

	while(iter != NULL)
	{
		if(iter->PID == pid)
			return iter;
		iter = os_itertasks(iter);
	}
	return NULL;
}

void os_enterirq(void)
{
	os_crit_t c = os_entercritical();
	os_irqnest++;
	os_exitcritical(c);
}

void os_exitirq(void)
{
	os_crit_t c = os_entercritical();
	os_irqnest--;
	if(os_irqnest == 0 && os_schedcore())
		os_cxtswitchisr();
	os_exitcritical(c);
}
