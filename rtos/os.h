#ifndef OS_H_
#define OS_H_

#include <stdint.h>
#include <stddef.h>
#include "os_arch.h"

/**
 * \addtogroup Types
 * \brief Base Types
 *
 * Core types and definitions used in the OS.
 * @{
 * \file
 */

/**
 * \brief Minimum priority level
 *
 * The minimum task priority level.
 * The idle task runs at this priority level.
 */
#define OS_MIN_PRIO (0)

/**
 * \brief Maximum priority level
 *
 * The maximum task priority level.
 */
#define OS_MAX_PRIO (7)

/**
 * \brief OS Error codes
 */
typedef enum os_error
{
	OS_OK = 0,        ///<Success
	OS_INVPARAM = -1, ///<Invalid parameter
	OS_INVCXT = -2,   ///<Invalid context
	OS_TIMEOUT = -3,  ///<Timer timeout
	OS_OWNED = -4     ///<Object already owned
} os_error;

/**
 * \brief OS Task states
 */
typedef enum os_taskstate
{
	RUNNING,      ///<Task currently running
	READY,        ///<Task ready to be run
	WAIT_TIMEOUT, ///<Task waiting for timeout
	WAIT_MUTEX,   ///<Task waiting for mutex unlock
	WAIT_SEM,     ///<Task waiting for semaphore increment
	WAIT_COND,    ///<Task waiting for condition
	KILLED        ///<Task no longer able to be run
} os_taskstate;

/**
 * \brief OS Timer object
 */
typedef struct os_timer
{
	uint32_t t;                     ///<Number of ticks till timeout
	void (*callback)(void*, void*); ///<Pointer to timer callback
	void *data;                     ///<Argument to callback
	void *data2;                    ///<Argument to callback

	struct os_timer *next;          ///<Pointer to next timer object
	struct os_timer *prev;          ///<Pointer to previous timer object
} os_timer;

/**
 * \brief OS Task Control Block
 */
typedef struct os_tcb
{
	uintptr_t SP;           ///<Saved stack pointer
	os_taskstate state;     ///<Task state
	uint8_t PID;            ///<Process ID
	uint8_t prio;           ///<Task priority
	const char *desc;       ///<Description string
	uintptr_t stk_base;     ///<Base of stack
	size_t stk_size;        ///<Stack size

	os_timer *timer;        ///<Timer that task is waiting on
	os_error ret;           ///<Timer Return value

	struct os_tcb *next;    ///<Pointer to next task in list
	struct os_tcb *prev;    ///<Pointer to previous task in list
	struct os_tcb *m_next;  ///<Pointer to next task in master list
	struct os_tcb *m_prev;  ///<Pointer to previous task in master list

	__attribute__((aligned(OS_STACK_ALIGN)))
	uint8_t stk_top[];      ///<Top of stack
} os_tcb;

/**
 * \brief OS TCB linked list object
 */
typedef struct os_tcblist
{
	os_tcb *head; ///<Pointer to list head
	os_tcb *tail; ///<Pointer to list tail
} os_tcblist;

/**
 * \brief OS TCB priority list object
 */
typedef struct os_priolist
{
	uint8_t bmp;        ///<Priority bitmap
	os_tcblist plst[8]; ///<::os_tcblist for each priority level
} os_priolist;

/** @}*/
#endif /* OS_H_ */
