# RTOS

A simple RTOS for ARM-Cortex microcontrollers written in C99.

## Features

- Priority scheduling with configurable tick rate.
- Round robin scheduling between tasks of equal priority.
- Static, compile-time allocation of all data structures.
- Lazy saving of floating-point registers.
- Task creation, destruction and suspension.
- Timers with callbacks.
- Mutexes with priority ceiling and optional timeouts.
- Semaphores with optional timeouts.
- Condition variables with optional timeouts.

## Platforms

|     Platform      |   Arch   | Support |            Notes            |
|-------------------|----------|---------|-----------------------------|
| ARM Cortex-M0/M0+ | ARMv6-M  | Full    |                             |
| ARM Cortex-M3     | ARMv7-M  | Full    | Does not use PRIMASK        |
| ARM Cortex-M4     | ARMv7E-M | Full    | Does not use PRIMASK        |
| ARM Cortex-M7     | ARMv7E-M | Full    | Double precision not tested |

## Build

To build libos.a:

`$ make`

To link to a project example:

`$ arm-none-eabi-gcc main.o libos.a`

or

`$ arm-none-eabi-gcc -L../rtos/build -los main.c`

To build the documentation (Doxygen required):

`$ make doc`

## Todo

- Per task time slice.
