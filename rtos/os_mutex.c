#include "os_mutex.h"
#include "os_core.h"
#include "os_list.h"
#include "os_timer.h"

static void os_mutexcallback(void *data, void *data2)
{
	os_tcb *task = data;
	os_mutex *mutex = data2;
	os_listremovetcb(&mutex->waiting, task);
	os_unblock(task, OS_TIMEOUT);
	os_sched();
}

void os_mutexinit(os_mutex *m)
{
	m->owner = NULL;
	os_listinit(&(m->waiting));
	m->prio_ceil = -1;
	m->prio_owner = 0;
}

os_error os_mutexinitprio(os_mutex *m, uint8_t prio_ceil)
{
	if(prio_ceil > OS_MAX_PRIO)
		return OS_INVPARAM;
	m->owner = NULL;
	os_listinit(&(m->waiting));
	m->prio_ceil = prio_ceil;
	m->prio_owner = 0;
	return OS_OK;
}

os_error os_mutexlock(os_mutex *m, uint32_t timeout)
{
	if(os_getrunningtask() == NULL) //Called from ISR
		return OS_INVCXT;

	os_crit_t c = os_entercritical();

	if(m->owner == NULL) //Take mutex
	{
		m->owner = os_getrunningtask();
		if(m->prio_ceil != -1)
			os_chprio(m->prio_ceil, &m->prio_owner);
		os_exitcritical(c);
		return OS_OK;
	}
	else if(m->owner == os_getrunningtask()) //Recursive lock
	{
		os_exitcritical(c);
		return OS_INVPARAM;
	}
	else //Locked by another task
	{
		if(timeout == 0) //No wait
		{
			os_exitcritical(c);
			return OS_OWNED;
		}

		os_tcb *task = os_getrunningtask();
		os_timer timer;
		timer.t = timeout;
		timer.callback = os_mutexcallback;
		timer.data = task;
		timer.data2 = m;
		os_block(WAIT_MUTEX, &m->waiting, (timeout != OS_TIMEOUT_INF) ? &timer: NULL);
		os_exitcritical(c);

		//Execution continues here
		return task->ret;
	}
}

os_error os_mutexunlock(os_mutex *m)
{
	if(os_getrunningtask() == NULL) //Called from ISR
		return OS_INVCXT;

	os_crit_t c = os_entercritical();
	if(m->owner == NULL) //Mutex not locked
	{
		os_exitcritical(c);
		return OS_INVPARAM;
	}
	if(m->owner != os_getrunningtask()) //Not owner
	{
		os_exitcritical(c);
		return OS_OWNED;
	}

	m->owner = NULL; //Relinquish ownership
	if(m->prio_ceil != -1)
		os_chprio(m->prio_owner, NULL);
	if(!os_listempty(&m->waiting)) //Tasks waiting on mutex
	{
		os_tcb *wake = os_listdequeue(&m->waiting); //FIFO
		if(m->prio_ceil != -1)
		{
			m->prio_owner = wake->prio;
			wake->prio = m->prio_ceil;
		}
		m->owner = wake;
		os_unblock(wake, OS_OK);
		os_sched();
	}
	os_exitcritical(c);
	return OS_OK;
}
