#ifndef OS_MUTEX_H_
#define OS_MUTEX_H_

#include "os.h"

/**
 * \addtogroup Mutex
 * \brief Functions to protect shared resources
 *
 * Provides blocking, non-recursive mutexes with optional timeouts and
 * priority ceiling.
 * @{
 * \file
 */

/**
 * \brief OS Mutex type
 */
typedef struct os_mutex
{
	os_tcb *owner;      ///<Pointer to mutex owner
	os_tcblist waiting; ///<List of tasks waiting on mutex
	int8_t prio_ceil;   ///<Priority ceiling (-1 for no ceiling)
	uint8_t prio_owner; ///<Original priority of mutex owner
} os_mutex;

/**
 * \brief Initialise mutex
 *
 * Must be called before using the mutex.
 * \param[in] *m Pointer to mutex
 */
void os_mutexinit(os_mutex *m);

/**
 * \brief Initialise mutex with priority ceiling
 *
 * Must be called before using the mutex.
 * \param[in] *m Pointer to mutex
 * \param[in] prio_ceil Priority ceiling of mutex
 * \retval ::OS_OK Success
 * \retval ::OS_INVPARAM Priority ceiling greater than ::OS_MAX_PRIO
 */
os_error os_mutexinitprio(os_mutex *m, uint8_t prio_ceil);

/**
 * \brief Macro to initialise a mutex
 *
 * Macro to initialise a mutex statically at compile time,
 * used as an alternative to ::os_mutexinit().
 */
#define OS_MUTEXINIT {NULL, {NULL, NULL}, -1, 0}

/**
 * \brief Macro to initialise a mutex with priority ceiling
 *
 * Macro to initialise a mutex with a priority ceiling statically at compile
 * time, used as an alternative to ::os_mutexinitprio().
 *
 * \warning prio parameter must be below or equal to ::OS_MAX_PRIO
 */
#define OS_MUTEXINITCEIL(prio) {NULL, {NULL, NULL}, (prio), 0}

/**
 * \brief Attempt to lock mutex
 *
 * If the mutex has no owner the mutex is locked and \c ::OS_OK is returned.
 * Additionally if the mutex has a priority ceiling the calling tasks priority
 * is set to the priority of the mutex.
 *
 * If the mutex cannot be locked the behaviour
 * depends on the value of \c timeout:
 *
 * - <tt>timeout == 0</tt>
 * 	- \c ::OS_OWNED is returned.
 * - <tt>timeout > 0</tt>
 * 	- The task is suspended until the mutex is unlocked or timeout occurs.
 * - <tt>timeout == ::OS_TIMEOUT_INF</tt>
 * 	- The task is suspended indefinitely until the mutex is unlocked.
 *
 * \warning Function cannot be called from an ISR.
 * \param[in] *m Pointer to mutex
 * \param[in] timeout Timeout to lock the mutex
 * \retval ::OS_OK Success
 * \retval ::OS_OWNED Mutex is owned by another task
 * \retval ::OS_TIMEOUT Timeout occurred before mutex could be locked
 * \retval ::OS_INVCXT Function was called from an ISR
 * \retval ::OS_INVPARAM Mutex was locked more than once by the same task
 */
os_error os_mutexlock(os_mutex *m, uint32_t timeout);

/**
 * \brief Unlocks mutex
 *
 * If the mutex is not owned by the calling task \c ::OS_OWNED if returned.
 *
 * If the mutex is owned by the calling task the mutex ownership is
 * relinquished and the first waiting task is woken up and given
 * ownership of the mutex, if any.
 *
 * Additionally if the mutex has a priority ceiling the priority of the
 * calling task is reverted to the value before the mutex was locked.
 * If a waiting task was woken its priority is raised to the value of the
 * mutex before it is unblocked.
 *
 * \warning Function cannot be called from an ISR.
 * \param[in] *m Pointer to mutex
 * \retval ::OS_OK Success
 * \retval ::OS_OWNED Calling task not owner of mutex
 * \retval ::OS_INVCXT Function was called from an ISR
 * \retval ::OS_INVPARAM Mutex was not locked
 */
os_error os_mutexunlock(os_mutex *m);

/** @}*/
#endif /* OS_MUTEX_H_ */
