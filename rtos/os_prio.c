#include "os_prio.h"
#include "os_arch.h"
#include "os_list.h"

void os_priolistinit(os_priolist *list)
{
	list->bmp = 0;
	for(int i = 0; i < 8; i++)
		os_listinit(&list->plst[i]);
}

void os_priolistenqueue(os_priolist *list, os_tcb *tcb)
{
	list->bmp |= (1 << tcb->prio);
	os_listenqueue(&list->plst[tcb->prio], tcb);
}

os_tcb *os_priolistpeek(os_priolist *list)
{
	return list->plst[os_fls(list->bmp)].head;
}

os_tcb *os_priolistdequeue(os_priolist *list)
{
	int priority = os_fls(list->bmp);
	os_tcb *ret = os_listdequeue(&list->plst[priority]);

	if(os_listempty(&list->plst[priority]))
		list->bmp &= ~(1 << priority); //Clear bitmap
	return ret;
}
