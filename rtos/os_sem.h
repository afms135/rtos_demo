#ifndef OS_SEM_H_
#define OS_SEM_H_

#include "os.h"

/**
 * \addtogroup Semaphore
 * \brief Functions for task signalling
 *
 * Provides blocking, semaphores with optional timeouts.
 * @{
 * \file
 */

/**
 * \brief OS Semaphore type
 */
typedef struct os_sem
{
	uint32_t v;         ///<Semaphore value
	os_tcblist waiting; ///<List of tasks waiting on semaphore
} os_sem;

/**
 * \brief Initialise a semaphore
 *
 * Initialises the semaphore \c s with the initial value \c v.
 *
 * Must be called before using the semaphore.
 * \param[in] *s Pointer to semaphore
 * \param[in] v Semaphore value
 */
void os_seminit(os_sem *s, uint32_t v);

/**
 * \brief Macro to initialise a semaphore
 *
 * Macro to initialise a semaphore statically at compile time,
 * used as an alternative to ::os_seminit().
 *
 * \param[in] v Semaphore value
 */
#define OS_SEMINIT(v) {(v), {NULL, NULL}}

/**
 * \brief Attempt to decrement semaphore
 *
 * When called from an ISR the function behaves
 * as if it were called with <tt>timeout == 0</tt>.
 *
 * If the semaphore value is above \c 0 it's value
 * is decremented and \c ::OS_OK is returned.
 *
 * If the semaphore cannot be decremented the
 * behaviour depends on the value of \c timeout:
 *
 * - <tt>timeout == 0</tt>
 * 	- The function returns \c ::OS_TIMEOUT immediately.
 * - <tt>timeout > 0</tt>
 * 	- The task is suspended until the semaphore is incremented
 * 	  or timeout occurs.
 * - <tt>timeout == ::OS_TIMEOUT_INF</tt>
 * 	- The task is suspended indefinitely until the semaphore is incremented.
 *
 * \warning ::os_enterirq must be called before this function if called within
 * an ISR.
 * \param[in] *s Pointer to the semaphore
 * \param[in] timeout Timeout to decrement the semaphore
 * \retval ::OS_OK Success
 * \retval ::OS_TIMEOUT Timeout occurred before semaphore could be decremented
 */
os_error os_semwait(os_sem *s, uint32_t timeout);

/**
 * \brief Increment semaphore
 *
 * Wakes up the first task waiting on the semaphore,
 * if there are none, the semaphore is incremented.
 * \param[in] *s Pointer to the semaphore
 * \retval ::OS_OK Success
 */
os_error os_semsignal(os_sem *s);

/** @}*/
#endif /* OS_SEM_H_ */
