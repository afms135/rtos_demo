#ifndef OS_PRIO_H_
#define OS_PRIO_H_

#include "os.h"

/**
 * \addtogroup Priority
 * \brief Functions to manipulate \c ::os_priolist priority queues
 *
 * @{
 * \file
 */

/**
 * \brief Initialise a priority list
 *
 * Initialises a \c ::os_priolist structure by setting the bitmap to zero
 * and setting the head and tail of each internal \c ::os_tcblist to NULL.
 *
 * \param[in] *list Pointer to list to be initialised
 */
void os_priolistinit(os_priolist *list);

/**
 * \brief Enqueue a tcb into a priority list
 *
 * Example:
 * \code
 * os_priolist list;
 * os_priolistinit(&list);
 * os_tcb *task = os_getrunningtask();
 *
 * os_priolistenqueue(&list, task);
 * \endcode
 * \param[in] *list Pointer to list to be updated
 * \param[in] *tcb Pointer to task to enqueue
 */
void os_priolistenqueue(os_priolist *list, os_tcb *tcb);

/**
 * \brief Return pointer to the highest priority task
 *
 * Returns a pointer to the highest priority task in the list.
 * Does not remove the tcb form the list.
 * \param[in] *list Pointer to list to inspect
 */
os_tcb *os_priolistpeek(os_priolist *list);

/**
 * \brief Dequeue highest priority tcb from priority list
 *
 * Returns a pointer to the dequeued \c os_tcb.
 * \param[in] *list Pointer to list to dequeue from
 */
os_tcb *os_priolistdequeue(os_priolist *list);

/**@}*/
#endif /* OS_PRIO_H_ */
