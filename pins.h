#ifndef PINS_H
#define PINS_H
#include <hw/gpio.h>

#define LED_ON()  (gpio_set(GPIO_PORTB, 3))
#define LED_OFF() (gpio_res(GPIO_PORTB, 3))

#define LCD_DC_HIGH() (gpio_set(GPIO_PORTB, 4))
#define LCD_DC_LOW()  (gpio_res(GPIO_PORTB, 4))
#define LCD_BL_HIGH() (gpio_set(GPIO_PORTA, 11))
#define LCD_BL_LOW()  (gpio_res(GPIO_PORTA, 11))

static inline void pins_init(void)
{
	gpio_enable(GPIO_PORTA);
	gpio_enable(GPIO_PORTB);

	//SPI
	gpio_mode(GPIO_PORTB, 5, GPIO_MODE_ALT); //MOSI
	gpio_mode(GPIO_PORTA, 5, GPIO_MODE_ALT); //SCK
	gpio_mode(GPIO_PORTA, 4, GPIO_MODE_ALT); //CS
	gpio_speed(GPIO_PORTB, 5, GPIO_SPEED_HIGH);
	gpio_speed(GPIO_PORTA, 5, GPIO_SPEED_HIGH);
	gpio_speed(GPIO_PORTA, 4, GPIO_SPEED_HIGH);
	gpio_altf(GPIO_PORTB, 5, 5);
	gpio_altf(GPIO_PORTA, 5, 5);
	gpio_altf(GPIO_PORTA, 4, 5);

	//I2C
	gpio_mode(GPIO_PORTB, 7, GPIO_MODE_ALT); //SDA
	gpio_mode(GPIO_PORTB, 6, GPIO_MODE_ALT); //SCL
	gpio_type(GPIO_PORTB, 7, GPIO_TYPE_OPENDRAIN);
	gpio_type(GPIO_PORTB, 6, GPIO_TYPE_OPENDRAIN);
	gpio_altf(GPIO_PORTB, 7, 4);
	gpio_altf(GPIO_PORTB, 6, 4);

	//UART
	gpio_mode(GPIO_PORTA,  2, GPIO_MODE_ALT); //TX
	gpio_mode(GPIO_PORTA, 15, GPIO_MODE_ALT); //RX
	gpio_altf(GPIO_PORTA,  2, 7);
	gpio_altf(GPIO_PORTA, 15, 7);

	//GPIO
	gpio_mode(GPIO_PORTB, 4, GPIO_MODE_OUTPUT); //DC
	gpio_set(GPIO_PORTB, 4);
	gpio_mode(GPIO_PORTA, 11, GPIO_MODE_OUTPUT); //BL
	gpio_res(GPIO_PORTA, 11);
	gpio_mode(GPIO_PORTB, 3, GPIO_MODE_OUTPUT); //LED
	gpio_res(GPIO_PORTB, 3);
}

#endif /*PINS_H*/
