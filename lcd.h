#ifndef LCD_H
#define LCD_H
#include <stdint.h>

#define RGB8(r,g,b) (((r & 0xF8) << 8) | ((g & 0xFC) << 3) | (b >> 3))
#define COL_BLACK   (0x0000)
#define COL_WHITE   (0xFFFF)
#define COL_RED     (0xF800)
#define COL_GREEN   (0x07E0)
#define COL_BLUE    (0x001F)
#define COL_CYAN    (0x07FF)
#define COL_MAGENTA (0xF81F)
#define COL_YELLOW  (0xFFE0)

#define LCD_WIDTH (160)
#define LCD_HEIGHT (80)

void lcd_init(void);
void lcd_window(uint16_t x0, uint16_t x1, uint16_t y0, uint16_t y1);
void lcd_raw(uint16_t *buf, uint16_t len);

void lcd_setpixel(uint16_t x, uint16_t y, uint16_t col);
void lcd_clear(uint16_t col);
void lcd_vline(uint16_t x, uint16_t y, uint16_t len, uint16_t col);
void lcd_hline(uint16_t x, uint16_t y, uint16_t len, uint16_t col);
void lcd_fill(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t col);
void lcd_outline(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t col);
void lcd_line(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t col);

void lcd_putchar(uint16_t x, uint16_t y, char c, uint16_t col);
void lcd_putstr(uint16_t x, uint16_t y, const char *str, uint16_t col);
__attribute__((format(printf, 4, 5)))
void lcd_printf(uint16_t x, uint16_t y, uint16_t col, const char *fmt, ...);

#endif /*LCD_H*/
