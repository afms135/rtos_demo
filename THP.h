#ifndef THP_H
#define THP_H

typedef struct thp_data
{
	float temp;
	float pres;
	float hum;
} thp_data;

void thp_init(void);
void thp_read(thp_data *d);

#endif /*THP_H*/
