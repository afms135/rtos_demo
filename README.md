# RTOS Demo

Demo of [RTOS](https://gitlab.com/afms135/rtos) running on a NUCLEO-F303K8
development board using the STM32F303K8T6 microcontroller running at 64MHz.

## Video Demo

[![Demo](https://img.youtube.com/vi/oNcYy4KKG6U/0.jpg)](https://www.youtube.com/watch?v=oNcYy4KKG6U)

## Features

- Priority scheduling used to maintain a constant framerate.
- Mutexes and semaphores used to lock shared peripherals and task signalling.
- Multiple tasks sharing the FPU.
- DMA used to suspend tasks while transfers are in progress.
- Command-line shell showing OS internal status.
- Compiled using O3 with link-time optimization.
- Microcontroller put to sleep when no tasks are running.

## Shell Screenshot

![Shell Screenshot](shell.png)

## Build Instructions

Requires the `arm-none-eabi-` tools in PATH, run `make` to build
`firmware.elf` and `firmware.hex`.
