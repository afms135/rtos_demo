#include <hw/gpio.h>
#include <hw/dma.h>
#include <hw/spi.h>
#include <hw/i2c.h>
#include <hw/uart.h>

#include <rtos/os_core.h>
#include <rtos/os_timer.h>
#include <rtos/os_mutex.h>
#include <rtos/os_sem.h>
#include <rtos/os_cond.h>

#include <pins.h>
#include <lcd.h>
#include <THP.h>
#include <obj.h>
#include <cmdshell.h>

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

/*
 * Globals/Defines
 */
#define VIEWPORT_X (80)
#define VIEWPORT_Y (5)
#define VIEWPORT_SIZ (60)
#define TOSCREEN(x, offset) (int)(x * (VIEWPORT_SIZ/2) + (VIEWPORT_SIZ/2) + offset + 0.5f)

thp_data thp;
int thp_rdy = 0;
os_mutex thp_lock = OS_MUTEXINIT;
os_cond thp_cond  = OS_CONDINIT;
os_mutex lcd_lock = OS_MUTEXINITCEIL(OS_MAX_PRIO);
os_sem lcd_signal = OS_SEMINIT(0);

/*
 * Tasks
 */
OS_TASK(render, 1024, arg)
{
	//Wait for lcd init
	os_semwait(&lcd_signal, OS_TIMEOUT_INF);

	float rot = 0.0f;
	int cur_shape = 0;
	int cnt = 0;
	while(1)
	{
		const obj *shape = shapes[cur_shape];

		os_mutexlock(&lcd_lock, OS_TIMEOUT_INF);

		//Clear viewport
		lcd_fill(VIEWPORT_X+1, VIEWPORT_Y+1, VIEWPORT_SIZ-2, VIEWPORT_SIZ-2, COL_BLACK);

		//Rotation
		vec3 rotated[MAX_VERTICES];
		float s = sinf(rot);
		float c = cosf(rot);
		rot += 0.05f;

		for(int i = 0; i < shape->num_vertices; i++)
		{
			float x = shape->vertex[i].x;
			float y = shape->vertex[i].y;
			float z = shape->vertex[i].z;

			rotated[i].x = c*c*x - c*s*y + s*z;
			rotated[i].y = s*x + c*y;
			rotated[i].z = (-c)*s*x + s*s*y + c*z;
		}

		//Draw edges
		for(int i = 0; i < shape->num_edges; i++)
		{
			int x0 = TOSCREEN(rotated[shape->edge[i][0]].x, VIEWPORT_X);
			int y0 = TOSCREEN(rotated[shape->edge[i][0]].y, VIEWPORT_Y);
			int x1 = TOSCREEN(rotated[shape->edge[i][1]].x, VIEWPORT_X);
			int y1 = TOSCREEN(rotated[shape->edge[i][1]].y, VIEWPORT_Y);
			lcd_line(x0, y0, x1, y1, shape->col);
		}

		os_mutexunlock(&lcd_lock);

		//get next shape
		cnt++;
		if(cnt == 100)
		{
			cnt = 0;
			cur_shape = (cur_shape + 1) % NUM_SHAPES;
		}

		//Cap max framerate
		os_suspend(OS_MSTOTICKS(40));
	}
}

OS_TASK(lcd, 1024, arg)
{
	//Init lcd, draw static objects
	lcd_init();
	lcd_clear(COL_BLACK);
	lcd_outline(0,0,LCD_WIDTH, LCD_HEIGHT, COL_WHITE);
	lcd_outline(2,2,LCD_WIDTH-4, LCD_HEIGHT-4, COL_WHITE);
	lcd_outline(VIEWPORT_X, VIEWPORT_Y, VIEWPORT_SIZ, VIEWPORT_SIZ, COL_WHITE);
	lcd_fill(10, 10, 10, 10, COL_RED);
	lcd_fill(20, 20, 10, 10, COL_GREEN);
	lcd_fill(30, 30, 10, 10, COL_BLUE);
	lcd_fill(40, 10, 10, 10, COL_CYAN);
	lcd_fill(50, 20, 10, 10, COL_MAGENTA);
	lcd_fill(60, 30, 10, 10, COL_YELLOW);
	os_semsignal(&lcd_signal);

	while(1)
	{
		if(os_mutexlock(&thp_lock, OS_TIMEOUT_INF) == OS_OK)
		{
			if(thp_rdy)
			{
				os_mutexlock(&lcd_lock, OS_TIMEOUT_INF);
				lcd_printf(10, 47, COL_WHITE, "T: %iC", (int)thp.temp);
				lcd_printf(10, 57, COL_WHITE, "P: %ihPa", (int)thp.pres/100);
				lcd_printf(10, 67, COL_WHITE, "H: %i%%", (int)thp.hum);
				os_mutexunlock(&lcd_lock);

				thp_rdy = 0;
				os_condbroadcast(&thp_cond);
			}
			os_mutexunlock(&thp_lock);
		}

		//Update load
		uint64_t ticks = os_getticks();
		uint64_t idle_ticks = os_getidleticks();
		int load = ((ticks-idle_ticks)*100)/ticks;

		os_mutexlock(&lcd_lock, OS_TIMEOUT_INF);
		lcd_printf(80, 67, COL_WHITE, "Load:%3i%%", load);
		os_mutexunlock(&lcd_lock);

		os_suspend(OS_MSTOTICKS(1000));
	}
}

OS_TASK(sensor, 1024, arg)
{
	thp_init();
	while(1)
	{
		os_mutexlock(&thp_lock, OS_TIMEOUT_INF);
		if(thp_rdy != 0)
			os_condwait(&thp_cond, &thp_lock, OS_TIMEOUT_INF);

		thp_read(&thp);
		thp_rdy = 1;

		//Signal new data is ready
		os_condbroadcast(&thp_cond);
		os_mutexunlock(&thp_lock);
		os_suspend(OS_MSTOTICKS(10000));
	}
}

OS_TASK(shell, 1024, arg)
{
	printf("\n\nOS Shell\n");
	printf("--------\n");
	while(1)
		shell_exec("os> ");
}

OS_TASK(led, 256, arg)
{
	while(1)
	{
		LED_ON();
		os_suspend(OS_MSTOTICKS(1000));
		LED_OFF();
		os_suspend(OS_MSTOTICKS(1000));
	}
}

/*
 * Main
 */
OS_STACK(idle_stk, 128);
int main(void)
{
	//Disable stdout buffering, to prevent waiting for a \n
	setvbuf(stdout, NULL, _IONBF, 0);

	//Peripheral init
	pins_init();
	dma_init();
	spi_init();
	i2c_init();
	uart_init(9600);

	//OS init
	os_init(idle_stk, sizeof(idle_stk));
	OS_TASKINIT(render, NULL, 5, "Render");
	OS_TASKINIT(lcd, NULL, 4, "LCD");
	OS_TASKINIT(sensor, NULL, 3, "Sensor");
	OS_TASKINIT(shell, NULL, 2, "Shell");
	OS_TASKINIT(led, NULL, 1, "LED");
	os_start();
	while(1);
}
