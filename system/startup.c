#include <stdint.h>

#define ALIAS(f) __attribute__ ((weak, alias (#f)))

extern int main(void);
extern void system_init(void);
extern uint32_t __etext;
extern uint32_t __data_start__;
extern uint32_t __data_end__;
extern uint32_t __bss_start__;
extern uint32_t __bss_end__;
extern uint32_t __StackTop;

//ARM Cortex M4 Interrupts
void Reset_Handler(void);
void NMI_Handler(void)                   ALIAS(DefaultHandler);
void HardFault_Handler(void)             ALIAS(DefaultHandler);
void MemManage_Handler(void)             ALIAS(DefaultHandler);
void BusFault_Handler(void)              ALIAS(DefaultHandler);
void UsageFault_Handler(void)            ALIAS(DefaultHandler);
void SVC_Handler(void)                   ALIAS(DefaultHandler);
void DebugMon_Handler(void)              ALIAS(DefaultHandler);
void PendSV_Handler(void)                ALIAS(DefaultHandler);
void SysTick_Handler(void)               ALIAS(DefaultHandler);
//STM32F303x8 Interrupts
void WWDG_IRQHandler(void)               ALIAS(DefaultHandler);
void PVD_IRQHandler(void)                ALIAS(DefaultHandler);
void TAMPER_STAMP_IRQHandler(void)       ALIAS(DefaultHandler);
void RTC_WKUP_IRQHandler(void)           ALIAS(DefaultHandler);
void FLASH_IRQHandler(void)              ALIAS(DefaultHandler);
void RCC_IRQHandler(void)                ALIAS(DefaultHandler);
void EXTI0_IRQHandler(void)              ALIAS(DefaultHandler);
void EXTI1_IRQHandler(void)              ALIAS(DefaultHandler);
void EXTI2_TS_IRQHandler(void)           ALIAS(DefaultHandler);
void EXTI3_IRQHandler(void)              ALIAS(DefaultHandler);
void EXTI4_IRQHandler(void)              ALIAS(DefaultHandler);
void DMA1_Channel1_IRQHandler(void)      ALIAS(DefaultHandler);
void DMA1_Channel2_IRQHandler(void)      ALIAS(DefaultHandler);
void DMA1_Channel3_IRQHandler(void)      ALIAS(DefaultHandler);
void DMA1_Channel4_IRQHandler(void)      ALIAS(DefaultHandler);
void DMA1_Channel5_IRQHandler(void)      ALIAS(DefaultHandler);
void DMA1_Channel6_IRQHandler(void)      ALIAS(DefaultHandler);
void DMA1_Channel7_IRQHandler(void)      ALIAS(DefaultHandler);
void ADC1_2_IRQHandler(void)             ALIAS(DefaultHandler);
void CAN_TX_IRQHandler(void)             ALIAS(DefaultHandler);
void CAN_RX0_IRQHandler(void)            ALIAS(DefaultHandler);
void CAN_RX1_IRQHandler(void)            ALIAS(DefaultHandler);
void CAN_SCE_IRQHandler(void)            ALIAS(DefaultHandler);
void EXTI9_5_IRQHandler(void)            ALIAS(DefaultHandler);
void TIM1_BRK_TIM15_IRQHandler(void)     ALIAS(DefaultHandler);
void TIM1_UP_TIM16_IRQHandler(void)      ALIAS(DefaultHandler);
void TIM1_TRG_COM_TIM17_IRQHandler(void) ALIAS(DefaultHandler);
void TIM1_CC_IRQHandler(void)            ALIAS(DefaultHandler);
void TIM2_IRQHandler(void)               ALIAS(DefaultHandler);
void TIM3_IRQHandler(void)               ALIAS(DefaultHandler);
void I2C1_EV_IRQHandler(void)            ALIAS(DefaultHandler);
void I2C1_ER_IRQHandler(void)            ALIAS(DefaultHandler);
void SPI1_IRQHandler(void)               ALIAS(DefaultHandler);
void USART1_IRQHandler(void)             ALIAS(DefaultHandler);
void USART2_IRQHandler(void)             ALIAS(DefaultHandler);
void USART3_IRQHandler(void)             ALIAS(DefaultHandler);
void EXTI15_10_IRQHandler(void)          ALIAS(DefaultHandler);
void RTC_Alarm_IRQHandler(void)          ALIAS(DefaultHandler);
void TIM6_DAC1_IRQHandler(void)          ALIAS(DefaultHandler);
void TIM7_DAC2_IRQHandler(void)          ALIAS(DefaultHandler);
void COMP2_IRQHandler(void)              ALIAS(DefaultHandler);
void COMP4_6_IRQHandler(void)            ALIAS(DefaultHandler);
void FPU_IRQHandler(void)                ALIAS(DefaultHandler);

//Interrupt vector table
__attribute__ ((used, section(".isr_vector")))
void (* const Vectors[])(void) =
{
	//Cortex-M4 Interrupts
	(void (*)(void))&__StackTop,   //Stack Pointer
	Reset_Handler,                 //Reset handler
	NMI_Handler,                   //NMI handler
	HardFault_Handler,             //Hard fault handler
	MemManage_Handler,             //MPU fault handler
	BusFault_Handler,              //Bus fault handler
	UsageFault_Handler,            //Usage fault handler
	0,0,0,0,                       //Reserved
	SVC_Handler,                   //SVCall handler
	DebugMon_Handler,              //Debug monitor handler
	0,                             //Reserved
	PendSV_Handler,                //PendSV handler
	SysTick_Handler,               //SysTick handler

	//STM32F303x8 Interrupts
	WWDG_IRQHandler,               //Window Watchdog
	PVD_IRQHandler,                //PVD through EXTI line 16 detection
	TAMPER_STAMP_IRQHandler,       //Tamper and TimeStamp through EXTI line 19
	RTC_WKUP_IRQHandler,           //RTC wakeup timer through EXTI line 20
	FLASH_IRQHandler,              //FLASH
	RCC_IRQHandler,                //RCC
	EXTI0_IRQHandler,              //EXTI Line0
	EXTI1_IRQHandler,              //EXTI Line1
	EXTI2_TS_IRQHandler,           //EXTI Line2 and Touch sensing
	EXTI3_IRQHandler,              //EXTI Line3
	EXTI4_IRQHandler,              //EXTI Line4
	DMA1_Channel1_IRQHandler,      //DMA1 channel 1
	DMA1_Channel2_IRQHandler,      //DMA1 channel 2
	DMA1_Channel3_IRQHandler,      //DMA1 channel 3
	DMA1_Channel4_IRQHandler,      //DMA1 channel 4
	DMA1_Channel5_IRQHandler,      //DMA1 channel 5
	DMA1_Channel6_IRQHandler,      //DMA1 channel 6
	DMA1_Channel7_IRQHandler,      //DMA1 channel 7
	ADC1_2_IRQHandler,             //ADC1 and ADC2
	CAN_TX_IRQHandler,             //CAN_TX
	CAN_RX0_IRQHandler,            //CAN_RX0
	CAN_RX1_IRQHandler,            //CAN_RX1
	CAN_SCE_IRQHandler,            //CAN_SCE
	EXTI9_5_IRQHandler,            //EXTI Line 9-15
	TIM1_BRK_TIM15_IRQHandler,     //TIM1 Break and TIM15 global
	TIM1_UP_TIM16_IRQHandler,      //TIM1 Update and TIM16 global
	TIM1_TRG_COM_TIM17_IRQHandler, //TIM1 Trigger and commutation and TIM17
	TIM1_CC_IRQHandler,            //TIM1 Capture Compare
	TIM2_IRQHandler,               //TIM2
	TIM3_IRQHandler,               //TIM3
	0,                             //Reserved
	I2C1_EV_IRQHandler,            //I2C1 Event and EXTI Line23
	I2C1_ER_IRQHandler,            //I2C1 Error
	0,0,                           //Reserved
	SPI1_IRQHandler,               //SPI1
	0,                             //Reserved
	USART1_IRQHandler,             //USART1 global and EXTI Line 25
	USART2_IRQHandler,             //USART2 global and EXTI Line 26
	USART3_IRQHandler,             //USART3 global and EXTI Line 28
	EXTI15_10_IRQHandler,          //EXTI Line 15-10
	RTC_Alarm_IRQHandler,          //RTC Alarm
	0,0,0,0,0,0,0,0,0,0,0,0,       //Reserved
	TIM6_DAC1_IRQHandler,          //TIM6 global and DAC1 underrun
	TIM7_DAC2_IRQHandler,          //TIM7 global and DAC2 underrun
	0,0,0,0,0,0,0,0,               //Reserved
	COMP2_IRQHandler,              //COMP2 and EXTI Line22
	COMP4_6_IRQHandler,            //COMP4 and COMP6 and EXTI 30 and 32
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //Reserved
	FPU_IRQHandler                 //Floating point
};

void Reset_Handler(void)
{
	uint32_t *src, *dst;

	//Copy data section from FLASH to RAM
	src = &__etext;
	dst = &__data_start__;
	while (dst < &__data_end__)
		*dst++ = *src++;

	//Clear BSS
	dst = &__bss_start__;
	while (dst < &__bss_end__)
		*dst++ = 0;

	//Enable FPU
	#define SCB_CPACR (*((volatile uint32_t*)0xE000ED88))
	SCB_CPACR |= (3 << 20) | (3 << 22);
	#undef SCB_CPACR

	//Init clocks and debug output
	system_init();

	//Jump to main()
	main();

	//Trap a returning main
	while(1);
}

void DefaultHandler(void)
{
	while(1);
}
