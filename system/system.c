#include <stm32f3xx.h>
#include <hw/uart.h>

/*
 * Clocks
 */
uint32_t AHB_Clock;
uint32_t APB1_Clock;
uint32_t APB2_Clock;

void system_init(void)
{
	//Setup flash waitstates and prefetcher
	FLASH->ACR &= ~FLASH_ACR_HLFCYA;
	FLASH->ACR |= FLASH_ACR_PRFTBE | FLASH_ACR_LATENCY_1;

	//Clock PLL to 64MHz from 8Mhz HSI (divided to 4Mhz before PLL)
	//Max AHB  = 72MHz (Use 64Mhz) x16
	//Max APB1 = 36MHz (Use 32Mhz) x16/2
	//Max APB2 = 72MHz (Use 64Mhz) x16
	RCC->CFGR |= RCC_CFGR_PLLMUL16 | RCC_CFGR_PPRE1_DIV2;
	RCC->CR |= RCC_CR_PLLON;
	while(!(RCC->CR & RCC_CR_PLLRDY));

	//Switch to PLL clock
	RCC->CFGR |= RCC_CFGR_SW_PLL;
	while((RCC->CFGR & RCC_CFGR_SWS_Msk) != RCC_CFGR_SWS_PLL);
	AHB_Clock = 64000000UL;
	APB1_Clock = 32000000UL;
	APB2_Clock = 64000000UL;
}

/*
 * I/O Redirection
 */
int _write(int file, char *buf, int nbytes)
{
	uart_send(buf, nbytes);
	return nbytes;
}
