#include "cmdshell.h"
#include <hw/uart.h>
#include <rtos/os_core.h>
#include <rtos/os_arch.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * Commands
 */
void cmd_help(char *arg)
{
	printf("Commands:\n");
	printf("  help - Print this help message\n");
	printf("  info - Print OS infomation\n");
	printf("    ps - Print task summary\n");
	printf("task n - Print task infomation\n");
	printf("  time - Print timer summary\n");
}

void cmd_info(char *arg)
{
	uint64_t ticks = os_getticks();
	uint64_t idle_ticks = os_getidleticks();

	printf("    Platform: "OS_PLATFORM"\n");
	printf("Architecture: "OS_ARCHITECTURE"\n");
	printf("        Load: %u%%\n", (int)(((ticks-idle_ticks)*100)/ticks));
	printf("    Built on: "__DATE__" "__TIME__"\n");
}

void cmd_ps(char *arg)
{
	const char *statestr[] = {"RUN","RDY","TIM","MTX","SEM","KIL"};
	puts("PID\tPrio\tState\tStack\tUsage\tDesc");
	puts("--------------------------------------------");

	os_tcb *iter = os_itertasks(NULL);
	while(iter != NULL)
	{
		printf("%u\t%u\t%s\t%u\t%u\t%s\n",
			iter->PID,
			iter->prio,
			statestr[iter->state],
			iter->stk_size,
			iter->stk_base - iter->SP,
			iter->desc);
		iter = os_itertasks(iter);
	}
}

void cmd_task(char *arg)
{
	const char *statestr[] = {
		"RUNNING",
		"READY",
		"WAIT_TIMEOUT",
		"WAIT_MUTEX",
		"WAIT_SEM",
		"KILLED"
	};

	if(arg == NULL)
	{
		printf("Usage: task <PID>\n");
		return;
	}

	os_tcb *tsk = os_gettcb(strtol(arg, NULL, 10));
	if(tsk == NULL)
	{
		printf("Error: No such task\n");
		return;
	}

	printf("          PID: %u\n", tsk->PID);
	printf("     Priority: %u\n", tsk->prio);
	printf("        State: %s\n", statestr[tsk->state]);
	printf("  Description: %s\n", tsk->desc);
	printf("   Stack Size: %u\n", tsk->stk_size);
	printf("    Stack Top: %p\n", tsk->stk_top);
	printf("Stack Pointer: %p\n", (void*)tsk->SP);
	printf("   Stack Base: %p\n", (void*)tsk->stk_base);
}

void cmd_time(char *arg)
{
	extern os_timer *os_waitlist;
	os_timer *iter = os_waitlist;
	uint32_t accum = 0;

	puts("Delta\tTimeout\tPID");
	puts("-------------------");
	while(iter != NULL)
	{
		accum += iter->t;
		os_tcb *tsk = iter->data;

		printf("%lu\t%lu\t%u\n",
			iter->t,
			accum,
			tsk->PID
		);
		iter = iter->next;
	}
}

typedef struct
{
	const char *str;
	void (*cmd)(char*);
} shell_cmdentry;

shell_cmdentry cmds[] = {
	{"help", cmd_help},
	{"info", cmd_info},
	{"ps"  , cmd_ps},
	{"task", cmd_task},
	{"time", cmd_time}
};

#define SHELL_BUFSIZ 50
#define NUM_CMDS (sizeof(cmds)/sizeof(shell_cmdentry))

/*
 * Shell
 */
static void shell_getline(char *buf, size_t size)
{
	size_t i = 0;
	while(1)
	{
		char c = uart_getc();

		if(c == 0x1B)	//Escape character
			continue;

		switch(c)
		{
		case '\r':
		case '\n':
			printf("\n"); //Newline
			buf[i] = '\0'; //Terminate
			return;
		case '\b':
		case 0x7F: //DEL
			if(i != 0)
			{
				i--;
				printf("\b \b"); //Rubout
			}
			break;
		default:
			if(i != size - 1) //Leave room for NULL terminator
			{
				putchar(c); //Echo
				buf[i] = c;
				i++;
			}
			break;
		}
	}
}

static void shell_strtok(char *buf, char **cmd, char **arg)
{
	*cmd = buf;
	*arg = NULL;

	while(*buf != '\0')
	{
		if(*buf == ' ')
		{
			*buf = '\0';
			*arg = buf+1;
			return;
		}
		buf++;
	}
}

void shell_exec(const char *prompt)
{
	char buf[SHELL_BUFSIZ];
	char *cmd = NULL;
	char *arg = NULL;

	//Prompt
	if(prompt != NULL)
		printf(prompt);

	//Get and tokenize input string
	shell_getline(buf, SHELL_BUFSIZ);
	shell_strtok(buf, &cmd, &arg);

	if(cmd[0] == '\0')
		return;

	for(size_t i = 0; i < NUM_CMDS; i++)
	{
		if(strcmp(cmds[i].str, cmd) == 0)
		{
			cmds[i].cmd(arg);
			printf("\r\n");
			return;
		}
	}

	printf("%s: Command not found\n", cmd);
}
