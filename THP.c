#include "THP.h"
#include <hw/i2c.h>
#include <rtos/os_timer.h>

//Defines
#define BME280_ID (0x60)
#define BME280_I2C_ADDR (0x76)
#define BME280_RST_MAGIC (0xB6)
#define BME280_STATUS_MEASURING (1<<3)
#define BME280_STATUS_IM_UPDATE (1<<0)

//Bit positions
//CTRL_HUM
#define BME280_OSRS_H_Pos (0)
//CTRL_MEAS
#define BME280_MODE_Pos (0)
#define BME280_OSRS_P_Pos (2)
#define BME280_OSRS_T_Pos (5)
//CONFIG
#define BME280_SPI3W_EN_Pos (0)
#define BME280_FILTER_Pos (2)
#define BME280_T_SB_Pos (5)

//Register addresses
#define BME280_ADDR_DATA (0xF7)
#define BME280_ADDR_CONFIG (0xF5)
#define BME280_ADDR_CTRL_MEAS (0xF4)
#define BME280_ADDR_STATUS (0xF3)
#define BME280_ADDR_CTRL_HUM (0xF2)
#define BME280_ADDR_CALIB2 (0xE1)
#define BME280_ADDR_RST (0xE0)
#define BME280_ADDR_ID (0xD0)
#define BME280_ADDR_CALIB1 (0x88)

//Calibration data
//Temperature
static uint16_t dig_T1;
static int16_t dig_T2;
static int16_t dig_T3;
//Pressure
static uint16_t dig_P1;
static int16_t dig_P2;
static int16_t dig_P3;
static int16_t dig_P4;
static int16_t dig_P5;
static int16_t dig_P6;
static int16_t dig_P7;
static int16_t dig_P8;
static int16_t dig_P9;
//Humidity
static uint8_t dig_H1;
static int16_t dig_H2;
static uint8_t dig_H3;
static int16_t dig_H4;
static int16_t dig_H5;
static int8_t dig_H6;

/*
 * Helpers
 */
static void bme280_wr(uint8_t addr, uint8_t val)
{
	//Send address + data combo
	uint8_t buf[] = {addr, val};
	i2c_send(BME280_I2C_ADDR, buf, sizeof(buf));
	i2c_stop();
}

static void bme280_rd(uint8_t addr, uint8_t *buf, uint8_t len)
{
	//Send address then read data
	i2c_send(BME280_I2C_ADDR, &addr, sizeof(addr));
	i2c_recv(BME280_I2C_ADDR, buf, len);
	i2c_stop();
}

/*
 * Compensation formulas, fixed point (from datasheet p25)
 */
// Returns temperature in DegC, resolution is 0.01 DegC. Output value of “5123” equals 51.23 DegC.
// t_fine carries fine temperature as global value
static int32_t t_fine;
static int32_t BME280_compensate_T_int32(int32_t adc_T)
{
	int32_t var1, var2, T;
	var1 = ((((adc_T>>3) - ((int32_t)dig_T1<<1))) * ((int32_t)dig_T2)) >> 11;
	var2 = (((((adc_T>>4) - ((int32_t)dig_T1)) * ((adc_T>>4) - ((int32_t)dig_T1))) >> 12) * ((int32_t)dig_T3)) >> 14;
	t_fine = var1 + var2;
	T = (t_fine * 5 + 128) >> 8;
	return T;
}

// Returns pressure in Pa as unsigned 32 bit integer in Q24.8 format (24 integer bits and 8 fractional bits).
// Output value of “24674867” represents 24674867/256 = 96386.2 Pa = 963.862 hPa
static uint32_t BME280_compensate_P_int64(int32_t adc_P)
{
	int64_t var1, var2, p;
	var1 = ((int64_t)t_fine) - 128000;
	var2 = var1 * var1 * (int64_t)dig_P6;
	var2 = var2 + ((var1*(int64_t)dig_P5)<<17);
	var2 = var2 + (((int64_t)dig_P4)<<35);
	var1 = ((var1 * var1 * (int64_t)dig_P3)>>8) + ((var1 * (int64_t)dig_P2)<<12);
	var1 = (((((int64_t)1)<<47)+var1))*((int64_t)dig_P1)>>33;
	if (var1 == 0)
	{
		return 0; // avoid exception caused by division by zero
	}
	p = 1048576-adc_P;
	p = (((p<<31)-var2)*3125)/var1;
	var1 = (((int64_t)dig_P9) * (p>>13) * (p>>13)) >> 25;
	var2 = (((int64_t)dig_P8) * p) >> 19;
	p = ((p + var1 + var2) >> 8) + (((int64_t)dig_P7)<<4);
	return(uint32_t)p;
}

// Returns humidity in %RH as unsigned 32 bit integer in Q22.10 format (22 integer and 10 fractional bits).
// Output value of “47445” represents 47445/1024 = 46.333 %RH
static uint32_t BME280_compensate_H_int32(int32_t adc_H)
{
	int32_t v_x1_u32r;

	v_x1_u32r = (t_fine - ((int32_t)76800));
	v_x1_u32r = (((((adc_H << 14) - (((int32_t)dig_H4) << 20) - (((int32_t)dig_H5) *
		v_x1_u32r)) + ((int32_t)16384)) >> 15) * (((((((v_x1_u32r *
		((int32_t)dig_H6)) >> 10) * (((v_x1_u32r * ((int32_t)dig_H3)) >> 11) +
		((int32_t)32768))) >> 10) + ((int32_t)2097152)) * ((int32_t)dig_H2) +
		8192) >> 14));
	v_x1_u32r = (v_x1_u32r - (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) *
		((int32_t)dig_H1)) >> 4));
	v_x1_u32r = (v_x1_u32r < 0 ? 0 : v_x1_u32r);
	v_x1_u32r = (v_x1_u32r > 419430400? 419430400: v_x1_u32r);
	return(uint32_t)(v_x1_u32r>>12);
}

/*
 * API
 */
void thp_init(void)
{
	//Check chip ID
	uint8_t ID;
	bme280_rd(BME280_ADDR_ID, &ID, sizeof(ID));
	if(ID != BME280_ID)
		return;

	//Reset chip
	bme280_wr(BME280_ADDR_RST, BME280_RST_MAGIC);
	os_suspend(OS_MSTOTICKS(50));

	//Wait for calibation data to be populated
	uint8_t status;
	do
		bme280_rd(BME280_ADDR_STATUS, &status, 1);
	while (status & BME280_STATUS_IM_UPDATE);

	//Read both sets of calibration data
	uint8_t buf1[26] = {0};
	uint8_t buf2[7] = {0};
	bme280_rd(BME280_ADDR_CALIB1, buf1, sizeof(buf1));
	bme280_rd(BME280_ADDR_CALIB2, buf2, sizeof(buf2));

	//Extract calibration constants
	//Temperature
	dig_T1 = (buf1[1] << 8) | buf1[0];
	dig_T2 = (buf1[3] << 8) | buf1[2];
	dig_T3 = (buf1[5] << 8) | buf1[4];
	//Pressure
	dig_P1 = (buf1[7] << 8) | buf1[6];
	dig_P2 = (buf1[9] << 8) | buf1[8];
	dig_P3 = (buf1[11] << 8) | buf1[10];
	dig_P4 = (buf1[13] << 8) | buf1[12];
	dig_P5 = (buf1[15] << 8) | buf1[14];
	dig_P6 = (buf1[17] << 8) | buf1[16];
	dig_P7 = (buf1[19] << 8) | buf1[18];
	dig_P8 = (buf1[21] << 8) | buf1[20];
	dig_P9 = (buf1[23] << 8) | buf1[22];
	//Humidity
	dig_H1 = buf1[25];
	dig_H2 = (buf2[1] << 8) | buf2[0];
	dig_H3 = buf2[2];
	dig_H4 = (buf2[3] << 4) | (buf2[4] & 0xF);
	dig_H5 = (buf2[5] << 4) | ((buf2[4] & 0xF0) >> 4);
	dig_H6 = buf2[6];

	//Config
	//Humidity oversampling=x1
	bme280_wr(BME280_ADDR_CTRL_HUM, (1 << BME280_OSRS_H_Pos));
	//Forced mode, no IIR filter
	bme280_wr(BME280_ADDR_CONFIG, 0);
	//Forced mode, x1 oversampling of pressure and temperature
	bme280_wr(BME280_ADDR_CTRL_MEAS,
		(1 << BME280_OSRS_T_Pos) |
		(1 << BME280_OSRS_P_Pos) |
		(1 << BME280_MODE_Pos));
}

void thp_read(thp_data *d)
{
	//Start a forced measurement
	bme280_wr(BME280_ADDR_CTRL_MEAS,
		(1 << BME280_OSRS_T_Pos) |
		(1 << BME280_OSRS_P_Pos) |
		(1 << BME280_MODE_Pos));

	//Wait for measurement end
	uint8_t status;
	do
		bme280_rd(BME280_ADDR_STATUS, &status, 1);
	while (status & BME280_STATUS_MEASURING);

	//Read data
	uint8_t buf[8] = {0};
	bme280_rd(BME280_ADDR_DATA, buf, sizeof(buf));

	//Extract data
	int32_t press = (buf[0] << 12) | (buf[1] << 4) | (buf[2] >> 4);
	int32_t temp = (buf[3] << 12) | (buf[4] << 4) | (buf[5] >> 4);
	int32_t hum = (buf[6] << 8) | buf[7];

	//Run compensation formulas (from datasheet)
	int32_t t = BME280_compensate_T_int32(temp);
	uint32_t p = BME280_compensate_P_int64(press);
	uint32_t h = BME280_compensate_H_int32(hum);

	//Fill struct
	d->temp = t / 100.0f;
	d->pres = p / 256.0f;
	d->hum = h / 1024.0f;
}
