#ifndef OBJ_H
#define OBJ_H

#define MAX_VERTICES (20)
#define NUM_SHAPES (5)

typedef struct
{
	float x,y,z;
} vec3;

typedef struct
{
	int num_vertices;
	int num_edges;
	const vec3 *vertex;
	const unsigned char (*edge)[2];
	unsigned short col;
} obj;

extern const obj * const shapes[NUM_SHAPES];

#endif /*OBJ_H*/
