#include "spi.h"
#include <hw/dma.h>
#include <stm32f3xx.h>

/*
 * Helper
 */
static void spi_waitend(void)
{
	while(!(SPI1->SR & SPI_SR_TXE));
	while(SPI1->SR & SPI_SR_BSY);
}

/*
 * API
 */
void spi_init(void)
{
	//Enable SPI1 clock
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;

	//SPI 32MHz, Mode 0, Master
	SPI1->CR1 |= SPI_CR1_MSTR;

	//8-bit, multi-master, set FIFO, enable DMA transmission
	SPI1->CR2 |= SPI_CR2_FRXTH | SPI_CR2_SSOE | SPI_CR2_TXDMAEN;

	//Enable
	SPI1->CR1 |= SPI_CR1_SPE;

	//Fastest speed
	spi_setdiv(SPI_DIV2);
}

void spi_setdiv(spi_div div)
{
	spi_waitend();
	SPI1->CR1 &= ~SPI_CR1_BR_Msk;
	SPI1->CR1 |= ((div & 7) << SPI_CR1_BR_Pos);
}

void spi_put8(uint8_t v)
{
	//8-bit
	SPI1->CR2 &= ~(SPI_CR2_DS_Msk);
	SPI1->CR2 |= (0x7 << SPI_CR2_DS_Pos);
	//8-bit store
	*(volatile uint8_t*)&(SPI1->DR) = v;

	//Wait for transaction end
	spi_waitend();
}

void spi_put16(uint16_t v)
{
	//16-bit
	SPI1->CR2 &= ~(SPI_CR2_DS_Msk);
	SPI1->CR2 |= (0xF << SPI_CR2_DS_Pos);
	SPI1->DR = v;

	//Wait for transaction end
	spi_waitend();
}

void spi_send(const uint8_t *buf, uint16_t len)
{
	//8-bit
	SPI1->CR2 &= ~(SPI_CR2_DS_Msk);
	SPI1->CR2 |= (0x7 << SPI_CR2_DS_Pos);

	//DMA
	dma_addr(DMA_CH3, (uintptr_t)buf, (uintptr_t)&SPI1->DR, len);
	dma_conf(DMA_CH3, DMA_PRIO_VHIGH, DMA_WIDTH_8, DMA_WIDTH_8, DMA_MINC | DMA_MTOP);
	dma_startandwait(DMA_CH3);

	//Wait for transaction end
	spi_waitend();
}

void spi_send16(const uint16_t *buf, uint16_t len)
{
	//16-bit
	SPI1->CR2 &= ~(SPI_CR2_DS_Msk);
	SPI1->CR2 |= (0xF << SPI_CR2_DS_Pos);

	//DMA
	dma_addr(DMA_CH3, (uintptr_t)buf, (uintptr_t)&SPI1->DR, len);
	dma_conf(DMA_CH3, DMA_PRIO_VHIGH, DMA_WIDTH_16, DMA_WIDTH_16, DMA_MINC | DMA_MTOP);
	dma_startandwait(DMA_CH3);

	//Wait for transaction end
	spi_waitend();
}

void spi_repeat(uint8_t v, uint16_t len)
{
	//8-bit
	SPI1->CR2 &= ~(SPI_CR2_DS_Msk);
	SPI1->CR2 |= (0x7 << SPI_CR2_DS_Pos);

	//DMA
	dma_addr(DMA_CH3, (uintptr_t)&v, (uintptr_t)&SPI1->DR, len);
	dma_conf(DMA_CH3, DMA_PRIO_VHIGH, DMA_WIDTH_8, DMA_WIDTH_8, DMA_MTOP);
	dma_startandwait(DMA_CH3);

	//Wait for transaction end
	spi_waitend();
}

void spi_repeat16(uint16_t v, uint16_t len)
{
	//16-bit
	SPI1->CR2 &= ~(SPI_CR2_DS_Msk);
	SPI1->CR2 |= (0xF << SPI_CR2_DS_Pos);

	//DMA
	dma_addr(DMA_CH3, (uintptr_t)&v, (uintptr_t)&SPI1->DR, len);
	dma_conf(DMA_CH3, DMA_PRIO_VHIGH, DMA_WIDTH_16, DMA_WIDTH_16, DMA_MTOP);
	dma_startandwait(DMA_CH3);

	//Wait for transaction end
	spi_waitend();
}
