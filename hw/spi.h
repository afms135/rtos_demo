#ifndef SPI_H
#define SPI_H
#include <stdint.h>

typedef enum
{
	SPI_DIV2 = 0,
	SPI_DIV4,
	SPI_DIV8,
	SPI_DIV16,
	SPI_DIV32,
	SPI_DIV64,
	SPI_DIV128,
	SPI_DIV256
} spi_div;

void spi_init(void);
void spi_setdiv(spi_div div);
void spi_put8(uint8_t v);
void spi_put16(uint16_t v);
void spi_send(const uint8_t *buf, uint16_t len);
void spi_send16(const uint16_t *buf, uint16_t len);
void spi_repeat(uint8_t v, uint16_t len);
void spi_repeat16(uint16_t v, uint16_t len);

#endif /*SPI_H*/
