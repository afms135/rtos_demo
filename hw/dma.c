#include "dma.h"
#include <rtos/os_core.h>
#include <rtos/os_timer.h>
#include <rtos/os_sem.h>
#include <stm32f3xx.h>

static DMA_Channel_TypeDef * const dmas[7] = {
	DMA1_Channel1, DMA1_Channel2, DMA1_Channel3, DMA1_Channel4,
	DMA1_Channel5, DMA1_Channel6, DMA1_Channel7
};
os_sem completed_sem[7];

/*
 * Interrupt handlers
 */
void dma_irqcore(dma_channel ch)
{
	os_enterirq();
	if(DMA1->ISR & (1 << ((ch * 4) + 1))) //Transfer completed
		os_semsignal(&completed_sem[ch]);
	dma_clearflags(ch); //Acknowledge interrupts
	os_exitirq();
}

void DMA1_Channel1_IRQHandler(void) {dma_irqcore(DMA_CH1);}
void DMA1_Channel2_IRQHandler(void) {dma_irqcore(DMA_CH2);}
void DMA1_Channel3_IRQHandler(void) {dma_irqcore(DMA_CH3);}
void DMA1_Channel4_IRQHandler(void) {dma_irqcore(DMA_CH4);}
void DMA1_Channel5_IRQHandler(void) {dma_irqcore(DMA_CH5);}
void DMA1_Channel6_IRQHandler(void) {dma_irqcore(DMA_CH6);}
void DMA1_Channel7_IRQHandler(void) {dma_irqcore(DMA_CH7);}

/*
 * API
 */
void dma_init(void)
{
	//Enable DMA clock
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;
	//Set high interrupt priority
	NVIC_SetPriority(DMA1_Channel1_IRQn, 0);
	NVIC_SetPriority(DMA1_Channel2_IRQn, 0);
	NVIC_SetPriority(DMA1_Channel3_IRQn, 0);
	NVIC_SetPriority(DMA1_Channel4_IRQn, 0);
	NVIC_SetPriority(DMA1_Channel5_IRQn, 0);
	NVIC_SetPriority(DMA1_Channel6_IRQn, 0);
	NVIC_SetPriority(DMA1_Channel7_IRQn, 0);
	//Enable channel interrupts
	NVIC_EnableIRQ(DMA1_Channel1_IRQn);
	NVIC_EnableIRQ(DMA1_Channel2_IRQn);
	NVIC_EnableIRQ(DMA1_Channel3_IRQn);
	NVIC_EnableIRQ(DMA1_Channel4_IRQn);
	NVIC_EnableIRQ(DMA1_Channel5_IRQn);
	NVIC_EnableIRQ(DMA1_Channel6_IRQn);
	NVIC_EnableIRQ(DMA1_Channel7_IRQn);
	//Init semaphores
	for(int i = 0; i < 7; i++)
		os_seminit(&completed_sem[i], 0);
}

void dma_clearflags(dma_channel ch)
{
	DMA1->IFCR = (0xF << (ch * 4));
}

void dma_enable(dma_channel ch)
{
	dmas[ch]->CCR |= DMA_CCR_TCIE | DMA_CCR_EN;
}

void dma_disable(dma_channel ch)
{
	dmas[ch]->CCR &= ~(DMA_CCR_TCIE | DMA_CCR_EN);
}

void dma_addr(dma_channel ch, uintptr_t mem, uintptr_t periph, uint16_t size)
{
	dmas[ch]->CMAR = mem;
	dmas[ch]->CPAR = periph;
	dmas[ch]->CNDTR = size;
}

void dma_conf(dma_channel ch, dma_prio prio, dma_width pwidth, dma_width mwidth, dma_flag flags)
{
	dmas[ch]->CCR =
		(prio << DMA_CCR_PL_Pos) |
		(pwidth << DMA_CCR_PSIZE_Pos) |
		(mwidth << DMA_CCR_MSIZE_Pos) |
		flags;
}

void dma_wait(dma_channel ch)
{
	os_semwait(&completed_sem[ch], OS_TIMEOUT_INF);
}

void dma_startandwait(dma_channel ch)
{
	dma_enable(ch);
	dma_wait(ch);
	dma_disable(ch);
}
