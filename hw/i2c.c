#include "i2c.h"
#include <hw/dma.h>
#include <stm32f3xx.h>

/*
 * API
 */
void i2c_init(void)
{
	//Enable clock
	RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;

	//Timing for 100kHz using 8MHz HSI (Datasheet section 28.4.9)
	I2C1->TIMINGR =
		(0x1 << I2C_TIMINGR_PRESC_Pos) |
		(0x4 << I2C_TIMINGR_SCLDEL_Pos) |
		(0x2 << I2C_TIMINGR_SDADEL_Pos) |
		(0xC3 << I2C_TIMINGR_SCLH_Pos) |
		(0xC7 << I2C_TIMINGR_SCLL_Pos);

	//Move I2C DMA channel to 4(TX) and 5(RX)
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
	SYSCFG->CFGR3 |=
		SYSCFG_CFGR3_I2C1_TX_DMA_RMP_1 |
		SYSCFG_CFGR3_I2C1_RX_DMA_RMP_1;

	//Enable I2C, DMA RX/TX
	I2C1->CR1 |= I2C_CR1_TXDMAEN | I2C_CR1_RXDMAEN | I2C_CR1_PE;
}

void i2c_send(uint8_t addr, const uint8_t *buf, uint8_t len)
{
	//Transfer config
	I2C1->CR2 =
		(len << I2C_CR2_NBYTES_Pos) |
		((addr & 0x7F) << 1);

	//DMA
	dma_addr(DMA_CH4, (uintptr_t)buf, (uintptr_t)(&I2C1->TXDR), len);
	dma_conf(DMA_CH4, DMA_PRIO_MED, DMA_WIDTH_8, DMA_WIDTH_8, DMA_MINC | DMA_MTOP);
	dma_enable(DMA_CH4);

	//Start
	I2C1->CR2 |= I2C_CR2_START;

	//Wait for transfer end
	dma_wait(DMA_CH4);
	dma_disable(DMA_CH4);

	//Wait for transfer complete
	while(!(I2C1->ISR & I2C_ISR_TC));
}

void i2c_recv(uint8_t addr, uint8_t *buf, uint8_t len)
{
	//Transfer config
	I2C1->CR2 =
		(len << I2C_CR2_NBYTES_Pos) |
		I2C_CR2_RD_WRN |
		((addr & 0x7F) << 1);

	//DMA
	dma_addr(DMA_CH5, (uintptr_t)buf, (uintptr_t)(&I2C1->RXDR), len);
	dma_conf(DMA_CH5, DMA_PRIO_MED, DMA_WIDTH_8, DMA_WIDTH_8, DMA_MINC | DMA_PTOM);
	dma_enable(DMA_CH5);

	//Start
	I2C1->CR2 |= I2C_CR2_START;

	//Wait for transfer end
	dma_wait(DMA_CH5);
	dma_disable(DMA_CH5);

	//Wait for transfer complete
	while(!(I2C1->ISR & I2C_ISR_TC));
}

void i2c_stop(void)
{
	I2C1->CR2 |= I2C_CR2_STOP;
}
