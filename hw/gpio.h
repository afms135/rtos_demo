#ifndef GPIO_H
#define GPIO_H

typedef enum gpio_port_t
{
	GPIO_PORTA = 0,
	GPIO_PORTB,
	GPIO_PORTC,
	GPIO_PORTD,
	GPIO_PORTF,
} gpio_port_t;

typedef enum gpio_mode_t
{
	GPIO_MODE_INPUT = 0,
	GPIO_MODE_OUTPUT,
	GPIO_MODE_ALT,
	GPIO_MODE_ANALOG
} gpio_mode_t;

typedef enum gpio_type_t
{
	GPIO_TYPE_PUSHPULL = 0,
	GPIO_TYPE_OPENDRAIN
} gpio_type_t;

typedef enum gpio_speed_t
{
	GPIO_SPEED_LOW = 0,
	GPIO_SPEED_MED,
	GPIO_SPEED_HIGH
} gpio_speed_t;

typedef enum gpio_pull_t
{
	GPIO_PULL_NONE = 0,
	GPIO_PULL_UP,
	GPIO_PULL_DOWN
} gpio_pull_t;

void gpio_enable(gpio_port_t port);
void gpio_mode(gpio_port_t port, int pin, gpio_mode_t mode);
void gpio_type(gpio_port_t port, int pin, gpio_type_t type);
void gpio_speed(gpio_port_t port, int pin, gpio_speed_t speed);
void gpio_pull(gpio_port_t port, int pin, gpio_pull_t pull);
void gpio_altf(gpio_port_t port, int pin, int af);
void gpio_set(gpio_port_t port, int pin);
void gpio_res(gpio_port_t port, int pin);
int gpio_val(gpio_port_t port, int pin);

#endif /*GPIO_H*/
