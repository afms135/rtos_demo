#ifndef UART_H
#define UART_H
#include <stdint.h>

void uart_init(int baud);
void uart_send(const char *buf, uint16_t len);
char uart_getc(void);

#endif /*UART_H*/
