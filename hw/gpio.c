#include "gpio.h"
#include <stm32f3xx.h>

static GPIO_TypeDef * const gpios[5] = { GPIOA, GPIOB, GPIOC, GPIOD, GPIOF };

/*
 * API
 */
void gpio_enable(gpio_port_t port)
{
	switch(port)
	{
	case GPIO_PORTA: RCC->AHBENR |= RCC_AHBENR_GPIOAEN; break;
	case GPIO_PORTB: RCC->AHBENR |= RCC_AHBENR_GPIOBEN; break;
	case GPIO_PORTC: RCC->AHBENR |= RCC_AHBENR_GPIOCEN; break;
	case GPIO_PORTD: RCC->AHBENR |= RCC_AHBENR_GPIODEN; break;
	case GPIO_PORTF: RCC->AHBENR |= RCC_AHBENR_GPIOFEN; break;
	}
}

void gpio_mode(gpio_port_t port, int pin, gpio_mode_t mode)
{
	gpios[port]->MODER &= ~(0x3 << (pin * 2));
	gpios[port]->MODER |= ((mode & 0x3) << (pin * 2));
}

void gpio_type(gpio_port_t port, int pin, gpio_type_t type)
{
	gpios[port]->OTYPER &= ~(1 << pin);
	gpios[port]->OTYPER |= ((type & 0x1) << pin);
}

void gpio_speed(gpio_port_t port, int pin, gpio_speed_t speed)
{
	gpios[port]->OSPEEDR &= ~(0x3 << (pin * 2));
	gpios[port]->OSPEEDR |= ((speed & 0x3) << (pin * 2));
}

void gpio_pull(gpio_port_t port, int pin, gpio_pull_t pull)
{
	gpios[port]->PUPDR &= ~(0x3 << (pin * 2));
	gpios[port]->PUPDR |= ((pull & 0x3) << (pin * 2));
}

void gpio_altf(gpio_port_t port, int pin, int af)
{
	if(pin < 8)
	{
		gpios[port]->AFR[0] &= ~(0xF << (pin * 4));
		gpios[port]->AFR[0] |= ((af & 0xF) << (pin * 4));
	}
	else
	{
		gpios[port]->AFR[1] &= ~(0xF << ((pin - 8) * 4));
		gpios[port]->AFR[1] |= ((af & 0xF) << ((pin - 8) * 4));
	}
}

void gpio_set(gpio_port_t port, int pin)
{
	gpios[port]->BSRR = (1 << (pin & 0xF));
}

void gpio_res(gpio_port_t port, int pin)
{
	gpios[port]->BRR = (1 << (pin & 0xF));
}

int gpio_val(gpio_port_t port, int pin)
{
	return gpios[port]->IDR & (1 << (pin & 0xF));
}
