#include "uart.h"
#include <hw/dma.h>
#include <rtos/os_core.h>
#include <rtos/os_timer.h>
#include <rtos/os_sem.h>
#include <stm32f3xx.h>

/*
 * Interrupt Handler
 */
os_sem char_recieved = OS_SEMINIT(0);
volatile char recieved;

void USART2_IRQHandler(void)
{
	os_enterirq();
	//Clear any overrun condition
	if(USART2->ISR & USART_ISR_ORE)
		USART2->ICR = USART_ICR_ORECF;

	//Character waiting?
	if(!(USART2->ISR & USART_ISR_RXNE))
		return;

	//Read character and signal
	recieved = USART2->RDR;
	os_semsignal(&char_recieved);
	os_exitirq();
}

/*
 * API
 */
void uart_init(int baud)
{
	//Enable clock
	RCC->APB1ENR |= RCC_APB1ENR_USART2EN;

	//Set baud rate, oversample by 16 (round up)
	extern uint32_t APB1_Clock;
	USART2->BRR = (APB1_Clock + baud / 2) / baud;

	//Enable DMA TX
	USART2->CR3 |= USART_CR3_DMAT;

	//RX TX enable, enable UART, enable RX interrupt
	USART2->CR1 |= USART_CR1_TE | USART_CR1_RE | USART_CR1_RXNEIE | USART_CR1_UE;

	//Enable RX interrupt high priority
	NVIC_SetPriority(USART2_IRQn, 0);
	NVIC_EnableIRQ(USART2_IRQn);
}

void uart_send(const char *buf, uint16_t len)
{
	dma_addr(DMA_CH7, (uintptr_t)buf, (uintptr_t)&USART2->TDR, len);
	dma_conf(DMA_CH7, DMA_PRIO_LOW, DMA_WIDTH_8, DMA_WIDTH_8, DMA_MINC | DMA_MTOP);
	dma_startandwait(DMA_CH7);

	//Wait for last byte to send
	while(!(USART2->ISR & USART_ISR_TXE));
}

char uart_getc(void)
{
	os_semwait(&char_recieved, OS_TIMEOUT_INF);
	return recieved;
}
