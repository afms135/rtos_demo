#ifndef DMA_H
#define DMA_H
#include <stdint.h>

typedef enum
{
	DMA_CH1 = 0,
	DMA_CH2,
	DMA_CH3,
	DMA_CH4,
	DMA_CH5,
	DMA_CH6,
	DMA_CH7
} dma_channel;

typedef enum
{
	DMA_WIDTH_8 = 0,
	DMA_WIDTH_16,
	DMA_WIDTH_32
} dma_width;

typedef enum
{
	DMA_PRIO_LOW = 0,
	DMA_PRIO_MED,
	DMA_PRIO_HIGH,
	DMA_PRIO_VHIGH
} dma_prio;

typedef enum
{
	DMA_MEM2MEM = (1<<14),
	DMA_MINC = (1<<7),
	DMA_PINC = (1<<6),
	DMA_CIRC = (1<<5),
	DMA_MTOP = (1<<4),
	DMA_PTOM = 0
} dma_flag;

void dma_init(void);
void dma_clearflags(dma_channel ch);
void dma_enable(dma_channel ch);
void dma_disable(dma_channel ch);
void dma_addr(dma_channel ch, uintptr_t mem, uintptr_t periph, uint16_t size);
void dma_conf(dma_channel ch, dma_prio prio, dma_width pwidth, dma_width mwidth, dma_flag flags);
void dma_wait(dma_channel ch);
void dma_startandwait(dma_channel ch);

#endif /*DMA_H*/
